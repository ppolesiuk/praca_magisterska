Require Import Metatheory.TypeSystem.
Require Systems.Fbounded.Base.
Require Systems.ExplicitFbounded.Base.
Require Systems.ExplicitFbounded.SubjectReduction.
Require Import ExplicitAndImplicit.Erase.
Require Import ExplicitAndImplicit.ImplicitSubjectReduction.

Require ExplicitAndImplicit.Simulation.
Require Translation.ExplicitStrongNorm.

Theorem Fbounded_strong_norm (V TV : Set) : 
  TypeSystemStrongNorm (Systems.Fbounded.Base.type_system V TV).
apply (simulation_sn 
  (Systems.Fbounded.Base.type_system V TV)
  (Systems.ExplicitFbounded.Base.type_system V TV)
  annotRel).
apply ExplicitAndImplicit.Simulation.EFbSimulation_ts.
apply FbSubjectReduction_ts.
apply Systems.ExplicitFbounded.SubjectReduction.EFbSubjectReduction_ts.
apply Translation.ExplicitStrongNorm.EFbStrongNorm_ts.
Qed.
