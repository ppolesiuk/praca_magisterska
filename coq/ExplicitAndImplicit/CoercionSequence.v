Require Import Metatheory.Relations.
Require Import Metatheory.VarSet.
Require Import ExplicitAndImplicit.Erase.
Require Import Systems.ExplicitFbounded.Inversion.

Inductive crc_seq (TV : Set) : Set :=
| cs_id   : crc_seq TV
| cs_cons : Ex.crc TV -> crc_seq TV -> crc_seq TV
.

Arguments cs_id   [TV].
Arguments cs_cons [TV] _ _.

Fixpoint cs_snoc {TV : Set} (cs : crc_seq TV) (c : Ex.crc TV) :=
  match cs with
  | cs_id => cs_cons c cs_id
  | cs_cons c0 cs => cs_cons c0 (cs_snoc cs c)
  end.

Fixpoint cs_app {V TV : Set} (cs : crc_seq TV) (e : Ex.exp V TV)
    : Ex.exp V TV :=
  match cs with
  | cs_id        => e
  | cs_cons c cs => Ex.exp_capp c (cs_app cs e)
  end.

Lemma cs_app_snoc {V TV : Set} : forall cs c (e : Ex.exp V TV),
    cs_app (cs_snoc cs c) e = cs_app cs (Ex.exp_capp c e).
Proof.
induction cs; intros c0 e; simpl.
reflexivity.
rewrite IHcs; reflexivity.
Qed.

Lemma annot_abs {V TV : Set} : forall (e' : Ex.exp V TV) T f,
  erase e' = Im.exp_abs T f -> exists cs f',
  e' = cs_app cs (Ex.exp_abs T f') /\ erase f' = f.
Proof.
induction e'; intros T f Herr; try discriminate Herr;
  simpl in Herr.
injection Herr; intros fEq TEq.
  exists cs_id; eexists; split; try eassumption.
  rewrite TEq; reflexivity.
edestruct IHe' as [ cs0 [f' [ HEq Herr' ]]]; try eassumption.
  eexists (cs_cons _ cs0); exists f'; split.
    rewrite HEq; reflexivity.
  assumption.
Qed.

Lemma annot_app {V TV : Set} : forall (e' : Ex.exp V TV) e1 e2,
  erase e' = Im.exp_app e1 e2 -> exists cs e1' e2',
  e' = cs_app cs (Ex.exp_app e1' e2') 
  /\ erase e1' = e1 /\ erase e2' = e2.
Proof.
induction e'; intros e1 e2 Herr; try discriminate Herr;
  simpl in Herr.
injection Herr; intros HEq2 HEq1.
  exists cs_id; eexists; eexists; repeat split; assumption.
edestruct IHe' as [cs0 [e1' [e2' [HEq [HErr1 HErr2]]]]]; try eassumption.
  eexists (cs_cons _ cs0); exists e1'; exists e2'; 
    repeat split; try eassumption.
  rewrite HEq; reflexivity.
Qed.

Lemma annot_tabs {V TV : Set} : forall (e' : Ex.exp V TV) U f,
  erase e' = Im.exp_tabs U f -> exists cs f',
  e' = cs_app cs (Ex.exp_tabs U f') /\ erase f' = f.
Proof.
induction e'; intros U f Herr; try discriminate Herr;
  simpl in Herr.
injection Herr; intros fEq UEq.
  exists cs_id; eexists; split; try eassumption.
  rewrite UEq; reflexivity.
edestruct IHe' as [ cs0 [f' [ HEq Herr' ]]]; try eassumption.
  eexists (cs_cons _ cs0); exists f'; split.
    rewrite HEq; reflexivity.
  assumption.
Qed.

Lemma annot_tapp {V TV : Set} : forall (e' : Ex.exp V TV) f S,
  erase e' = Im.exp_tapp f S -> exists cs f' c,
  e' = cs_app cs (Ex.exp_tapp f' S c) /\ erase f' = f.
Proof.
induction e'; intros f S Herr; try discriminate Herr;
  simpl in Herr.
injection Herr; intros SEq fEq.
  exists cs_id; eexists; eexists; split; try eassumption.
  subst; reflexivity.
edestruct IHe' as [cs0 [f' [c' [ HEq Herr']]]]; try eassumption.
  eexists (cs_cons _ cs0); exists f'; exists c'; split.
    rewrite HEq; reflexivity.
  assumption.
Qed.

Lemma erase_cs_app {V TV : Set} : forall cs (e : Ex.exp V TV),
  erase (cs_app cs e) = erase e.
Proof.
induction cs; intuition.
Qed.

Lemma cs_app_well_typed {V TV : Set} : forall cs (e : Ex.exp V TV),
  Ex.well_typed (cs_app cs e) -> Ex.well_typed e.
Proof.
induction cs; simpl; intros e Hwt.
assumption.
destruct Hwt as [ Gamma T Htp ]; inversion Htp.
  apply IHcs; econstructor; eassumption.
Qed.

Lemma cs_app_context_cl {V TV : Set} : forall cs (e1 e2 : Ex.exp V TV) R,
  Ex.context_cl R e1 e2 ->
  Ex.context_cl R (cs_app cs e1) (cs_app cs e2).
Proof.
induction cs; intros e1 e2 R Hred; simpl.
assumption.
apply Ex.CCL_CApp; auto.
Qed.

Lemma cs_inversion_abs {V TV : Set} : 
  forall cs Gamma (e : Ex.exp (inc V) TV) T0 T,
  Ex.typing Gamma (cs_app cs (Ex.exp_abs T0 e)) T ->
  T = Ex.typ_top \/ exists T1 T2, T = Ex.typ_arrow T1 T2.
Proof.
induction cs; intros Gamma e T0 T Htp.
inversion Htp; eauto.
inversion Htp.
  edestruct IHcs as [ Htop | [ S1 [ S2 Harrow ]]]; try eassumption; subst.
  left; eapply sub_inversion_top; eassumption.
  edestruct (@sub_inversion_arrow); eauto.
Qed.

Lemma cs_inversion_tabs {V TV : Set} : 
  forall cs Gamma (e : Ex.exp V (inc TV)) U T,
  Ex.typing Gamma (cs_app cs (Ex.exp_tabs U e)) T ->
  T = Ex.typ_top \/ exists T1 T2, T = Ex.typ_all T1 T2.
Proof.
induction cs; intros Gamma e T0 T Htp.
inversion Htp; eauto.
inversion Htp.
  edestruct IHcs as [ Htop | [ S1 [ S2 Hall ]]]; try eassumption; subst.
  left; eapply sub_inversion_top; eassumption.
  edestruct (@sub_inversion_all); eauto.
Qed.
