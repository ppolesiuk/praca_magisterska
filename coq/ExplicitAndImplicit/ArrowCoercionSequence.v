Require Import Arith.
Require Import Metatheory.Relations.
Require Import Metatheory.VarSet.
Require Import ExplicitAndImplicit.Erase.
Require Import ExplicitAndImplicit.CoercionSequence.

Import Ex.

Inductive ArrowForm {TV : Set} : crc_seq TV -> Prop :=
| AF_id   : ArrowForm cs_id
| AF_cons : forall c1 c2 cs,
    ArrowForm cs ->
    ArrowForm (cs_cons (crc_arrow c1 c2) cs)
.

Module CoercionWeigth.

Fixpoint crc_weight {TV : Set} (c : crc TV) :=
  match c with
  | crc_id         => 0
  | crc_comp c1 c2 => 2 + crc_weight c1 + crc_weight c2
  | crc_top        => 0
  | crc_var _      => 0
  | crc_arrow _ _  => 0
  | crc_all _ _    => 0
  end.

Fixpoint cs_weight {TV : Set} (cs : crc_seq TV) :=
  match cs with
  | cs_id        => 0
  | cs_cons c cs => 1 + crc_weight c + cs_weight cs
  end.

Lemma weight_ind {TV : Set} (P : crc_seq TV -> Prop) :
  (forall cs, cs_weight cs = 0 -> P cs) ->
  (forall n, (forall cs, cs_weight cs = n -> P cs) ->
    forall cs, cs_weight cs = 1 + n -> P cs) ->
  forall cs, P cs.
Proof.
intros H0 Hn cs.
assert (H : exists n, cs_weight cs = n) by (eexists; reflexivity).
destruct H as [ n H ].
generalize H; generalize cs; clear H cs.
induction n.
  assumption.
  apply Hn; assumption.
Qed.

End CoercionWeigth.
Import CoercionWeigth.

Lemma arrow_form_exists {V TV : Set} :
  forall cs T (f : exp (inc V) TV) Gamma T1 T2,
  typing Gamma (cs_app cs (exp_abs T f)) (typ_arrow T1 T2) ->
  exists cs', ArrowForm cs' /\
    iota_red^* (cs_app cs (exp_abs T f)) (cs_app cs' (exp_abs T f)).
Proof.
refine (weight_ind _ _ _).

intros cs Hw T f Gamma T1 T2 Htp.
destruct cs.
  exists cs_id; split; [ constructor | constructor 1 ].
discriminate Hw.

intros n IHn cs Hw T f Gamma T1 T2 Htp; destruct cs.
  discriminate Hw.
inversion_clear Htp; rename H into Htp; rename H0 into Hsub.
inversion Hsub.
(* c = crc_id *)
rewrite <- H in Hw; rewrite H1 in Htp; injection Hw; intro Hwn.
edestruct IHn as [ cs' [ AFcs' Hred ]]; try eassumption.
exists cs'; split.
  assumption.
  simpl; econstructor 3.
    apply CCL_Step; constructor.
    assumption.
(* c = crc_comp *)
rewrite <- H1 in Hw.
injection Hw; intro Hwn.
edestruct (IHn (cs_cons c1 (cs_cons c2 cs))) 
  as [ cs' [ AFcs' Hred ]]; try eassumption.
  rewrite <- Hwn; simpl.
    rewrite <- plus_Snm_nSm; rewrite plus_assoc; reflexivity.
  simpl; repeat econstructor; eassumption.
exists cs'; split.
  assumption.
simpl; econstructor 3.
  apply CCL_Step. constructor.
apply Hred.
(* c = crc_var *)
edestruct (@cs_inversion_abs) as [ Htop | [TT1 [ TT2 Harrow ]]]; eauto.
  subst; discriminate.
  subst; discriminate.
(* c = crc_arrow *)
rewrite <- H1 in Hw; rewrite <- H2 in Htp.
injection Hw; intro Hwn.
edestruct (IHn cs) as [ cs' [ AFcs' Hred ]]; try eassumption.
eexists (cs_cons (crc_arrow _ _) cs'); split.
  constructor; assumption.
simpl; apply ccl_star_ccl_lemma.
apply CCL_CApp; apply CCL_Step; assumption.
Qed.

Lemma arrow_form_reduce {V TV : Set} : forall cs, ArrowForm cs ->
  forall (e1 e2 : exp V TV), exists cs1 cs2, 
  iota_red^* 
    (exp_app (cs_app cs e1) e2) 
    (cs_app cs1 (exp_app e1 (cs_app cs2 e2))).
Proof.
induction 1; simpl; intros e1 e2.
exists cs_id; exists cs_id; constructor 1.
edestruct IHArrowForm as [cs1 [cs2 Hred]].
eexists (cs_cons _ cs1); eexists (cs_snoc cs2 _).
econstructor 3.
  apply CCL_Step; constructor.
simpl; apply ccl_star_ccl_lemma; apply CCL_CApp; apply CCL_Step.
rewrite cs_app_snoc; apply Hred.
Qed.
