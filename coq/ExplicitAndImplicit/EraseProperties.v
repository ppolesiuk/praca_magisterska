Require Import Metatheory.VarSet.
Require Import Metatheory.Misc.
Require Import Systems.FboundedCommon.Base.
Require Import Systems.FboundedCommon.MetaProperties.
Require Import ExplicitAndImplicit.Erase.

Lemma erase_pres_sub {TV : Set} : forall c : Ex.crc TV, forall Gamma T S,
  Ex.sub Gamma c T S -> Im.sub Gamma T S.
Proof.
induction 1.
constructor 1.
econstructor 2; eassumption.
constructor 3.
constructor 4; assumption.
constructor 5; assumption.
constructor 6; assumption.
Qed.

Lemma erase_pres_typing {V TV : Set} : forall (e : Ex.exp V TV) Gamma T,
  Ex.typing Gamma e T -> Im.typing Gamma (erase e) T.
Proof.
induction 1; simpl; econstructor; eauto.
eapply erase_pres_sub; eassumption.
eapply erase_pres_sub; eassumption.
Qed.

Ltac destruct_exists :=
  match goal with
  | [ H : exists _ : _, _ |- _ ] => destruct H
  end.

Ltac destruct_conj :=
  match goal with
  | [ H : _ /\ _ |- _ ] => destruct H
  end.

Lemma coercion_exists {TV : Set} : forall Gamma (T S : typ TV),
  Im.sub Gamma T S -> exists c, Ex.sub Gamma c T S.
Proof.
induction 1; repeat destruct_exists; eexists.
constructor 1.
econstructor 2; eassumption.
constructor 3.
constructor 4; assumption.
constructor 5; eassumption.
constructor 6; eassumption.
Qed.

Lemma exp_exists {V TV : Set} : forall (e : Im.exp V TV) Gamma T,
  Im.typing Gamma e T -> exists e', erase e' = e /\ Ex.typing Gamma e' T.
Proof.
induction 1; repeat destruct_exists; repeat destruct_conj.
eexists (Ex.exp_var _); split.
  reflexivity.
  constructor; assumption.
eexists (Ex.exp_abs _ _); split.
  subst; reflexivity.
  constructor; assumption.
eexists (Ex.exp_app _ _); split.
  subst; reflexivity.
  econstructor; eassumption.
eexists (Ex.exp_tabs _ _); split.
  subst; reflexivity.
  constructor; assumption.
edestruct (@coercion_exists) as [c Hsub].
  eassumption.
  eexists (Ex.exp_tapp _ _ c); split.
  subst; reflexivity.
  econstructor; eassumption.
edestruct (@coercion_exists) as [c Hsub].
  eassumption.
  eexists (Ex.exp_capp c _); split.
  subst; reflexivity.
  econstructor; eassumption.
Qed.

Lemma erase_map_exp_t {V TV : Set} : forall e : Ex.exp V TV,
  forall (TW : Set) (f : TV -> TW),
  erase (Ex.map_exp_t f e) = Im.map_exp_t f (erase e).
Proof.
induction e; intros; simpl.
reflexivity.
rewrite IHe; reflexivity.
rewrite IHe1; rewrite IHe2; reflexivity.
rewrite IHe; reflexivity.
rewrite IHe; reflexivity.
rewrite IHe; reflexivity.
Qed.

Lemma erase_map_exp_e {V TV : Set} : forall e : Ex.exp V TV,
  forall (W : Set) (f : V -> W),
  erase (Ex.map_exp_e f e) = Im.map_exp_e f (erase e).
Proof.
induction e; intros; simpl.
reflexivity.
rewrite IHe; reflexivity.
rewrite IHe1; rewrite IHe2; reflexivity.
rewrite IHe; reflexivity.
rewrite IHe; reflexivity.
rewrite IHe; reflexivity.
Qed.

Lemma erase_bind_exp_t {V TV : Set} : forall e : Ex.exp V TV,
  forall (TW : Set) (f : TV -> Ex.typ TW * Ex.crc TW) (g : TV -> Im.typ TW)
     (Hfg : forall a, f_fst f a = g a),
  erase (Ex.bind_exp_t f e) = Im.bind_exp_t g (erase e).
Proof.
induction e; intros; simpl.
reflexivity.
erewrite IHe; try erewrite bind_typ_ext; try reflexivity; assumption.
erewrite IHe1; try erewrite IHe2; eauto.
erewrite IHe; try erewrite bind_typ_ext; try reflexivity.
  intro a; destruct a; simpl; trivial; rewrite Hfg; reflexivity.
  intro a; destruct a; simpl; trivial.
    unfold f_fst; unfold Ex.shift_tc; simpl; rewrite Hfg; reflexivity.
erewrite IHe; try erewrite bind_typ_ext; eauto.
erewrite IHe; eauto.
Qed.

Lemma erase_bind_exp_e {V TV : Set} : forall e : Ex.exp V TV,
  forall (W : Set) (f : V -> Ex.exp W TV) (g : V -> Im.exp W TV)
     (Hfg : forall x, erase (f x) = g x),
  erase (Ex.bind_exp_e f e) = Im.bind_exp_e g (erase e).
Proof.
induction e; intros; simpl.
auto.
erewrite IHe; auto.
  intro x; destruct x; simpl; trivial.
  rewrite erase_map_exp_e; rewrite Hfg; reflexivity.
erewrite IHe1; try erewrite IHe2; eauto.
erewrite IHe; try reflexivity.
  intro x; unfold Ex.shift_et.
  rewrite erase_map_exp_t; rewrite Hfg; reflexivity.
erewrite IHe; eauto.
erewrite IHe; eauto.
Qed.

Lemma erase_subst_exp_e {V TV : Set} : forall e1 (e2 : Ex.exp V TV),
  erase (Ex.subst_exp_e e1 e2) = Im.subst_exp_e (erase e1) (erase e2).
Proof.
intros; apply erase_bind_exp_e.
  intro x; destruct x; reflexivity.
Qed.

Lemma erase_subst_exp_t {V TV : Set} : forall (e : Ex.exp V (inc TV)) T c,
  erase (Ex.subst_exp_t e T c) = Im.subst_exp_t (erase e) T.
Proof.
intros; apply erase_bind_exp_t.
  intro a; destruct a; reflexivity.
Qed.
