Require Import Metatheory.RelationProperties.
Require Import ExplicitAndImplicit.Erase.
Require Import ExplicitAndImplicit.EraseProperties.
Require Import ExplicitAndImplicit.Simulation.
Require Import Systems.ExplicitFbounded.SubjectReduction.

Theorem ImplicitSubjectReduction {V TV : Set} : 
  forall (e1 e2 : Im.exp V TV),
  Im.beta_red e1 e2 -> forall Gamma T, 
    Im.typing Gamma e1 T -> Im.typing Gamma e2 T.
Proof.
intros e1 e2 Hbeta Gamma T Htp.
edestruct (@exp_exists) as [e1' [Herr1 Htp']]; [ eassumption | ].
destruct (EFbSimulation e1 e2 Hbeta e1') as [e2' [Herr2 Hred']].
  econstructor; eassumption.
  assumption.
rewrite <- Herr2; apply erase_pres_typing.
eapply (rcomp_pres (fun e' => Ex.typing Gamma e' T)); try eassumption.
  apply rtc_pres.
  intros; eapply SubjectReduction_iota; eassumption.
intros; eapply SubjectReduction_beta; eassumption.
Qed.

Require Import Metatheory.TypeSystem.

Corollary FbSubjectReduction_ts {V TV : Set} :
  TypeSystemSubjectReduction (Im.type_system V TV).
Proof.
intros e1 Htp e2 Hred; destruct Htp; econstructor.
eapply ImplicitSubjectReduction; [ eapply Hred | eassumption ].
Qed.
