Require Systems.ExplicitFbounded.Base.
Require Systems.Fbounded.Base.

Module Ex := Systems.ExplicitFbounded.Base.
Module Im := Systems.Fbounded.Base.

Fixpoint erase {V TV : Set} (e : Ex.exp V TV) : Im.exp V TV :=
  match e with
  | Ex.exp_var x      => Im.exp_var x
  | Ex.exp_abs T e    => Im.exp_abs T (erase e)
  | Ex.exp_app e1 e2  => Im.exp_app (erase e1) (erase e2)
  | Ex.exp_tabs U e   => Im.exp_tabs U (erase e)
  | Ex.exp_tapp e T c => Im.exp_tapp (erase e) T
  | Ex.exp_capp c e   => erase e
  end.

Definition annotRel {V TV : Set} (e : Im.exp V TV) (e' : Ex.exp V TV) : Prop :=
  erase e' = e.
