Require Import Arith.
Require Import Metatheory.Relations.
Require Import Metatheory.VarSet.
Require Import ExplicitAndImplicit.Erase.
Require Import ExplicitAndImplicit.CoercionSequence.
Require Import ExplicitAndImplicit.ArrowCoercionSequence.

Import Ex.
Import CoercionWeigth.

Inductive QuantifierForm {TV : Set} : crc_seq TV -> Prop :=
| AF_id   : QuantifierForm cs_id
| AF_cons : forall c1 c2 cs,
    QuantifierForm cs ->
    QuantifierForm (cs_cons (crc_all c1 c2) cs)
.

Lemma quantifier_form_exists {V TV : Set} :
  forall cs U (f : exp V (inc TV)) Gamma T1 T2,
  typing Gamma (cs_app cs (exp_tabs U f)) (typ_all T1 T2) ->
  exists cs', QuantifierForm cs' /\
    iota_red^* (cs_app cs (exp_tabs U f)) (cs_app cs' (exp_tabs U f)).
Proof.
refine (weight_ind _ _ _).

intros cs Hw U f Gamma T1 T2 Htp.
destruct cs.
  exists cs_id; split; [ constructor | constructor 1 ].
discriminate Hw.

intros n IHn cs Hw U f Gamma T1 T2 Htp; destruct cs.
  discriminate Hw.
inversion_clear Htp; rename H into Htp; rename H0 into Hsub.
inversion Hsub.
(* c = crc_id *)
rewrite <- H in Hw; rewrite H1 in Htp; injection Hw; intro Hwn.
edestruct IHn as [ cs' [ AFcs' Hred ]]; try eassumption.
exists cs'; split.
  assumption.
  simpl; econstructor 3.
    apply CCL_Step; constructor.
    assumption.
(* c = crc_comp *)
rewrite <- H1 in Hw.
injection Hw; intro Hwn.
edestruct (IHn (cs_cons c1 (cs_cons c2 cs))) 
  as [ cs' [ AFcs' Hred ]]; try eassumption.
  rewrite <- Hwn; simpl.
    rewrite <- plus_Snm_nSm; rewrite plus_assoc; reflexivity.
  simpl; repeat econstructor; eassumption.
exists cs'; split.
  assumption.
simpl; econstructor 3.
  apply CCL_Step. constructor.
apply Hred.
(* c = crc_var *)
edestruct (@cs_inversion_tabs) as [ Htop | [TT1 [ TT2 Hall ]]]; eauto.
  subst; discriminate.
  subst; discriminate.
(* c = crc_all *)
rewrite <- H1 in Hw; rewrite <- H2 in Htp.
injection Hw; intro Hwn.
edestruct (IHn cs) as [ cs' [ AFcs' Hred ]]; try eassumption.
eexists (cs_cons (crc_all _ _) cs'); split.
  constructor; assumption.
simpl; apply ccl_star_ccl_lemma.
apply CCL_CApp; apply CCL_Step; assumption.
Qed.

Lemma quantifier_form_reduce {V TV : Set} : forall cs, QuantifierForm cs ->
  forall (e : exp V TV) S c, exists cs1 c', 
  iota_red^* 
    (exp_tapp (cs_app cs e) S c) 
    (cs_app cs1 (exp_tapp e S c')).
Proof.
induction 1; simpl; intros e S c.
exists cs_id; exists c; constructor 1.
edestruct IHQuantifierForm as [cs1 [cs2 Hred]].
eexists (cs_cons _ cs1); eexists _.
econstructor 3.
  apply CCL_Step; constructor; reflexivity.
simpl; apply ccl_star_ccl_lemma; apply CCL_CApp; apply CCL_Step.
apply Hred.
Qed.
