Require Import Metatheory.Relations.
Require Import ExplicitAndImplicit.Erase.
Require Import ExplicitAndImplicit.EraseProperties.
Require Import ExplicitAndImplicit.CoercionSequence.
Require Import ExplicitAndImplicit.ArrowCoercionSequence.
Require Import ExplicitAndImplicit.QuantifierCoercionSequence.

Lemma EFbSimulationStep {V TV : Set} : forall e1 e2 : Im.exp V TV,
  Im.beta e1 e2 -> forall e1', Ex.well_typed e1' ->
  erase e1' = e1 -> exists e2', erase e2' = e2 /\ 
  rcomp (Ex.iota_red ^*) Ex.beta_red e1' e2'.
Proof.
induction 1; subst; intros e1' Hwt Herr.

(* Beta-E case *)
edestruct (annot_app e1') as [cs2 [f0' [a' [HEq [Herr_f0' Herr_a']]]]];
    [ eassumption | ].
edestruct (annot_abs f0') as [csf [f' [HEqf0 Herr_f']]]; [ eassumption | ].
rewrite HEq in Hwt.
destruct (cs_app_well_typed _ _ Hwt) as [Gamma TT Htp]; inversion_clear Htp.
edestruct (arrow_form_exists csf T f') as [csf' [AFcsf' Red_csf]].
  rewrite_all HEqf0; eassumption.
destruct (arrow_form_reduce csf' AFcsf' (Ex.exp_abs T f') a') 
  as [cs2' [cs1 Red_csf']].
eexists (cs_app cs2 (cs_app cs2' (Ex.subst_exp_e f' (cs_app cs1 a')))).
split.
  repeat (rewrite erase_cs_app).
  rewrite erase_subst_exp_e.
  rewrite erase_cs_app.
  subst; reflexivity.
subst; eexists; split.
  apply Ex.ccl_star_ccl_lemma; apply cs_app_context_cl; apply Ex.CCL_Step.
  eapply rtc_trans.
    apply Ex.ccl_star_ccl_lemma; apply Ex.CCL_App1; apply Ex.CCL_Step.
    eassumption.
  eassumption.
apply cs_app_context_cl; apply cs_app_context_cl; apply Ex.CCL_Step.
  constructor; reflexivity.
(* Beta-T case *)
edestruct (annot_tapp e1') as [cs2 [f0' [c [HEq Herr_f0']]]];
    [ eassumption | ].
edestruct (annot_tabs f0') as [csf [f' [HEqf0 Herr_f']]]; 
    [ eassumption | ].
rewrite HEq in Hwt.
destruct (cs_app_well_typed _ _ Hwt) as [Gamma TT Htp]; inversion_clear Htp.
edestruct (quantifier_form_exists csf U f') as [csf' [QFcsf' Red_csf]].
  rewrite_all HEqf0; eassumption.
destruct (quantifier_form_reduce csf' QFcsf' (Ex.exp_tabs U f') T c) 
  as [cs2' [c0 Red_csf']].
eexists (cs_app cs2 (cs_app cs2' (Ex.subst_exp_t f' _ _))).
split.
  repeat (rewrite erase_cs_app).
  subst; apply erase_subst_exp_t.
subst; eexists; split.
  apply Ex.ccl_star_ccl_lemma; apply cs_app_context_cl; apply Ex.CCL_Step.
  eapply rtc_trans.
    apply Ex.ccl_star_ccl_lemma; apply Ex.CCL_TApp; apply Ex.CCL_Step.
    eassumption.
  eassumption.
apply cs_app_context_cl; apply cs_app_context_cl; apply Ex.CCL_Step.
  constructor; reflexivity.
Qed.

Theorem EFbSimulation {V TV : Set} : forall e1 e2 : Im.exp V TV,
  Im.beta_red e1 e2 -> forall e1', Ex.well_typed e1' ->
  erase e1' = e1 -> exists e2', erase e2' = e2 /\ 
  rcomp (Ex.iota_red ^*) Ex.beta_red e1' e2'.
Proof.
induction 1; intros ee1' Hwt Herr.
  eapply EFbSimulationStep; eassumption.
(* Next cases are very similar. *)
edestruct (annot_abs ee1') as [ cs [ f' [ HEq Hferr]]]; [ eassumption | ].
  edestruct IHcontext_cl as [f2' [Hferr2 Hfred]].
    rewrite HEq in Hwt; 
      destruct (cs_app_well_typed _ _ Hwt) as [ Gamma TT Htp]; inversion Htp.
      econstructor; eassumption.
    assumption.
  exists (cs_app cs (Ex.exp_abs T f2')); split.
    rewrite erase_cs_app; simpl; rewrite Hferr2; reflexivity.
  rewrite HEq.
  destruct Hfred as [ ee' [ Hfred1 Hfred2 ]]; eexists; split.
  apply Ex.ccl_star_ccl_lemma; apply cs_app_context_cl.
    apply Ex.CCL_Abs; apply Ex.CCL_Step; eassumption.
  apply cs_app_context_cl; apply Ex.CCL_Abs; assumption.
edestruct (annot_app ee1') as [cs [ef' [ea' [HEq [Herrf Herra]]]]];
    [ eassumption | ].
  edestruct (IHcontext_cl ef') as [ef2' [Herrf2 Hf2red]].
    rewrite HEq in Hwt;
      destruct (cs_app_well_typed _ _ Hwt) as [ Gamma TT Htp]; inversion Htp.
      econstructor; eassumption.
    assumption.
  exists (cs_app cs (Ex.exp_app ef2' ea')); split.
    rewrite erase_cs_app; simpl; subst; reflexivity.
  rewrite HEq.
  destruct Hf2red as [ ee' [ Hfred1 Hfred2 ]]; eexists; split.
  apply Ex.ccl_star_ccl_lemma; apply cs_app_context_cl.
    apply Ex.CCL_App1; apply Ex.CCL_Step; eassumption.
  apply cs_app_context_cl; apply Ex.CCL_App1; assumption.
edestruct (annot_app ee1') as [cs [ef' [ea' [HEq [Herrf Herra]]]]];
    [ eassumption | ].
  edestruct (IHcontext_cl ea') as [ea2' [Herra2 Ha2red]].
    rewrite HEq in Hwt;
      destruct (cs_app_well_typed _ _ Hwt) as [ Gamma TT Htp]; inversion Htp.
      econstructor; eassumption.
    assumption.
  exists (cs_app cs (Ex.exp_app ef' ea2')); split.
    rewrite erase_cs_app; simpl; subst; reflexivity.
  rewrite HEq.
  destruct Ha2red as [ ee' [ Hared1 Hared2 ]]; eexists; split.
  apply Ex.ccl_star_ccl_lemma; apply cs_app_context_cl.
    apply Ex.CCL_App2; apply Ex.CCL_Step; eassumption.
  apply cs_app_context_cl; apply Ex.CCL_App2; assumption.
edestruct (annot_tabs ee1') as [cs [f' [HEq Hferr]]]; [ eassumption | ].
  edestruct IHcontext_cl as [f2' [Hferr2 Hfred]].
    rewrite HEq in Hwt; 
      destruct (cs_app_well_typed _ _ Hwt) as [ Gamma TT Htp]; inversion Htp.
      econstructor; eassumption.
    assumption.
  eexists (cs_app cs (Ex.exp_tabs _ f2')); split.
    rewrite erase_cs_app; simpl; rewrite Hferr2; reflexivity.
  rewrite HEq.
  destruct Hfred as [ ee' [ Hfred1 Hfred2 ]]; eexists; split.
  apply Ex.ccl_star_ccl_lemma; apply cs_app_context_cl.
    apply Ex.CCL_TAbs; apply Ex.CCL_Step; eassumption.
  apply cs_app_context_cl; apply Ex.CCL_TAbs; assumption.
edestruct (annot_tapp ee1') as [cs [f' [c' [HEq Hferr]]]]; [ eassumption | ].
  edestruct IHcontext_cl as [f2' [Hferr2 Hfred]].
    rewrite HEq in Hwt; 
      destruct (cs_app_well_typed _ _ Hwt) as [ Gamma TT Htp]; inversion Htp.
      econstructor; eassumption.
    assumption.
  eexists (cs_app cs (Ex.exp_tapp f2' _ c')); split.
    rewrite erase_cs_app; simpl; rewrite Hferr2; reflexivity.
  rewrite HEq.
  destruct Hfred as [ ee' [ Hfred1 Hfred2 ]]; eexists; split.
  apply Ex.ccl_star_ccl_lemma; apply cs_app_context_cl.
    apply Ex.CCL_TApp; apply Ex.CCL_Step; eassumption.
  apply cs_app_context_cl; apply Ex.CCL_TApp; assumption.
Qed.

Require Import Metatheory.TypeSystem.

Corollary EFbSimulation_ts {V TV : Set} :
  TypeSystemSimulation (Im.type_system V TV) (Ex.type_system V TV) annotRel.
Proof.
split; simpl.
intros e Hwt; destruct Hwt as [Gamma T Htp].
edestruct (@exp_exists) as [e' [Herr Htp']].
  eassumption.
eexists; split.
  econstructor; eassumption.
  assumption.

intros e1 e2 Hwt_e1 Hred e1' Hwt_e1' Herr.
edestruct (EFbSimulation e1 e2) as [ e2' [ Herr2 Hred']]; try eassumption.
exists e2'; split; trivial.
destruct Hred' as [enf' [Hiota Hbeta]].
clear Hwt_e1' Herr Herr2; induction Hiota.
  constructor 1; constructor 1; assumption.
  econstructor 2.
    constructor 2; eassumption.
    constructor 1; constructor 1; assumption.
  econstructor 2.
    constructor 2; eassumption.
    auto.
Qed.
