Require Import Metatheory.Relations.
Require Import Metatheory.Simulation.

Record TypeSystem : Type :=
  { term  : Type
  ; valid : term -> Prop
  ; red   : term -> term -> Prop
  }.

Record TypeSystemSimulation 
    (T1 T2 : TypeSystem) 
    (sim : Relation (term T1) (term T2)) 
    : Prop :=
  { exists_valid    : forall M, valid T1 M -> 
      exists M', valid T2 M' /\ sim M M'
  ; simulation_step : forall M N, valid T1 M -> red T1 M N ->
      forall M', valid T2 M' -> sim M M' -> 
      exists N', sim N N' /\ (red T2)^+ M' N'
  }.

Arguments exists_valid [T1][T2][sim] _ _ _.

Definition TypeSystemSubjectReduction (TS : TypeSystem) :=
  forall M : term TS, valid TS M ->
  forall N : term TS, red TS M N -> valid TS N.

Definition TypeSystemStrongNorm (TS : TypeSystem) :=
  forall M : term TS, valid TS M -> StrongNorm (red TS) M.

Lemma SubjectReduction_plus (TS : TypeSystem) :
  TypeSystemSubjectReduction TS ->
  forall M : term TS, valid TS M ->
  forall N : term TS, (red TS)^+ M N -> valid TS N.
Proof.
intros Hsr M Mvalid N Hred; induction Hred; eauto.
Qed.

Module WellTypedTerms.

Definition WTT (TS : TypeSystem) : Type :=
  { M : term TS | valid TS M }.

Definition WTT_red (TS : TypeSystem) : WTT TS -> WTT TS -> Prop :=
  fun M N => red TS (proj1_sig M) (proj1_sig N).

Lemma WTT_red_plus (TS : TypeSystem) :
  TypeSystemSubjectReduction TS ->
  forall M N, (red TS)^+ (proj1_sig M) (proj1_sig N) ->
    (WTT_red TS)^+ M N.
Proof.
intro Hsr.
assert (H : forall m n, (red TS)^+ m n ->
  forall M N, proj1_sig M = m -> proj1_sig N = n -> 
    (WTT_red TS)^+ M N).
induction 1.
constructor 1; subst; assumption.
intros M N; destruct M as [xM xMvalid]; simpl.
intros HEq1 HEq2; rewrite_all <- HEq1.
assert (yvalid : valid TS y).
  eapply Hsr; eassumption.
econstructor 2.
  instantiate (1 := exist _ y yvalid).
  assumption.
apply IHtrans_cl; auto.
eauto.
Qed.

End WellTypedTerms.
Import WellTypedTerms.

Lemma simulation_sn (T1 T2 : TypeSystem)
    (sim : Relation (term T1) (term T2))
    : TypeSystemSimulation T1 T2 sim -> 
      TypeSystemSubjectReduction T1 ->
      TypeSystemSubjectReduction T2 ->
      TypeSystemStrongNorm T2 -> 
      TypeSystemStrongNorm T1.
Proof.
intros Hsim Hsr1 Hsr2 HSN2 M Mvalid.

eapply (SimulationLemma _ (WTT_red T1) (fun M N => M = proj1_sig N)).
intros x1 x2 y1 Hred HEq; destruct y1; simpl in HEq; rewrite_all HEq.
assert (Hvalid2 : valid T1 x2) by
  (apply (Hsr1 x); assumption).
exists (exist _ x2 Hvalid2); split.
  constructor 1; assumption.
  reflexivity.
instantiate (1 := exist _ M Mvalid); reflexivity.

destruct (exists_valid Hsim M Mvalid) as [N [Nvalid simMN]].
eapply (SimulationLemma _ (WTT_red T2) 
  (fun M N => sim (proj1_sig M) (proj1_sig N))).
intros X1 X2 Y1; 
  destruct X1 as [x1 x1valid];
  destruct X2 as [x2 x2valid];
  destruct Y1 as [y1 y1valid]; simpl.
intros Hred simxy1; unfold WTT_red in Hred; simpl in Hred.
edestruct (simulation_step _ _ _ Hsim x1) as [ y2 [ simxy2 Hred2]];
    try eassumption.
assert (y2valid : valid T2 y2).
  eapply (SubjectReduction_plus _ Hsr2 y1); assumption.
exists (exist _ y2 y2valid); simpl; split.
  apply WTT_red_plus; assumption.
assumption.
instantiate (1 := exist _ N Nvalid); assumption.

eapply (SimulationLemma _ (red T2) (fun M N => proj1_sig M = N)).
intros X1 X2 y1;
  destruct X1 as [x1 x1valid];
  destruct X2 as [x2 x2valid]; simpl.
intros Hred HEq; unfold WTT_red in Hred; simpl in Hred.
rewrite <- HEq; clear HEq.
eexists; split.
  constructor 1; eassumption.
  reflexivity.
simpl; reflexivity.

apply HSN2; assumption.
Qed.