
Definition Relation (X Y : Type) := X -> Y -> Prop.

Inductive trans_cl {X : Type} (R : Relation X X) (x : X) : X -> Prop :=
| TC_Step : forall y,
    R x y -> trans_cl R x y
| TC_Trans : forall y z,
    R x y -> trans_cl R y z -> trans_cl R x z
.

Notation "R ^+" := (trans_cl R) (at level 8).

Inductive refl_trans_cl {X : Type} (R : Relation X X) (x : X) : X -> Prop :=
| RTC_Relf : refl_trans_cl R x x
| RTC_Step : forall y,
    R x y -> refl_trans_cl R x y
| RTC_Trans : forall y z,
    R x y -> refl_trans_cl R y z -> refl_trans_cl R x z
.

Notation "R ^*" := (refl_trans_cl R) (at level 8).

Definition union {X Y : Type} (R1 R2 : Relation X Y) : Relation X Y :=
  fun x y => R1 x y \/ R2 x y.
Definition rcomp {X Y Z : Type} (R1 : Relation X Y) (R2 : Relation Y Z) 
    : Relation X Z :=
  fun x z => exists y, R1 x y /\ R2 y z.

CoInductive InfiniteReductionPath {X : Type} (R : Relation X X) (x : X) 
    : Prop :=
| IRP_Step : forall y,
    R x y ->
    InfiniteReductionPath R y ->
    InfiniteReductionPath R x
.

Definition StrongNorm {X : Type} (R : Relation X X) (x : X) :=
  ~InfiniteReductionPath R x.

Lemma rtc_trans {X : Type} (R : Relation X X) :
  forall x y z, R^* x y -> R^* y z -> R^* x z.
Proof.
intros x y z HR1; generalize z; clear z; induction HR1.
auto.
intros; eapply RTC_Trans; eassumption.
intros; eapply RTC_Trans; eauto.
Qed.