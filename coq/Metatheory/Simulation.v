Require Import Metatheory.Relations.

Definition Simulation {X Y : Type}
  (RX : Relation X X) (RY : Relation Y Y) (Sim : Relation X Y) : Prop :=
  forall x1 x2 y1, RX x1 x2 -> Sim x1 y1 -> 
  exists y2, RY^+ y1 y2 /\ Sim x2 y2.

Lemma inf_path_tc {X : Type} (R : Relation X X) :
  forall x : X, InfiniteReductionPath R^+ x ->
    InfiniteReductionPath R x.
Proof.
cofix; intros x IPx; destruct IPx as [y Rxy IPy].
destruct Rxy.
econstructor; eauto.
econstructor; try eassumption;
  apply inf_path_tc; econstructor; eassumption.
Qed.

Theorem SimulationLemma {X Y : Type}
  (RX : Relation X X) (RY : Relation Y Y) (Sim : Relation X Y)
  (HSim : Simulation RX RY Sim) :
  forall x y, Sim x y -> StrongNorm RY y -> StrongNorm RX x.
Proof.
intros x y Sxy SNy IPx; apply SNy.
apply inf_path_tc.
clear SNy; generalize x y Sxy IPx; clear x y Sxy IPx; cofix.
intros x y Sxy IPx; destruct IPx as [xn Rxn IPxn].
destruct (HSim x xn y Rxn Sxy) as [yf [RedY Snf]].
econstructor; eauto.
Qed.
