
Inductive inc (V : Set) : Set :=
| VZ : inc V
| VS : V -> inc V
.

Arguments VZ [V].
Arguments VS [V] _.

Definition map_inc {V W : Set} (f : V -> W) (v : inc V) : inc W :=
  match v with
  | VZ   => VZ
  | VS v => VS (f v)
  end.