
Definition f_fst {A B C : Set} (f : A -> B * C) (x : A) : B :=
  fst (f x).

Definition f_snd {A B C : Set} (f : A -> B * C) (x : A) : C :=
  snd (f x).
