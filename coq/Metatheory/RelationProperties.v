Require Import Metatheory.Relations.

Lemma rcomp_pres {X : Type} : forall (P : X -> Prop)
  (R1 : Relation X X) (R2 : Relation X X),
  (forall x y, R1 x y -> P x -> P y) ->
  (forall x y, R2 x y -> P x -> P y) ->
  (forall x y, rcomp R1 R2 x y -> P x -> P y).
Proof.
intros P R1 R2 PP1 PP2 x y Hr Px.
destruct Hr as [z [HR1 HR2]].
eauto.
Qed.

Lemma rtc_pres {X : Type} : forall (P : X -> Prop)
  (R : Relation X X),
  (forall x y, R x y -> P x -> P y) ->
  (forall x y, R^* x y -> P x -> P y).
Proof.
intros P R PP x y Hr; induction Hr; eauto.
Qed.