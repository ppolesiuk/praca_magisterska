COQC="coqc"
MAIN="Main.v"
SPECIALDIRS="Metatheory"

MAPDIRS=""
for dir in $(find . -mindepth 1 -maxdepth 1 -type d -printf "%P ")
do
	MAPDIRS+=" -R $dir $dir"
done

echo > Makefile

echo ".PHONY: all $SPECIALDIRS main clean" >> Makefile
echo >> Makefile

ALL=""
for file in $(find . -iname "*.v")
do
	ALL+=" ${file}o"
done
echo "all:$ALL" >> Makefile
echo >> Makefile

for special in $SPECIALDIRS
do
	DEP=""
	for file in $(find $special -iname "*.v")
	do
		DEP+=" ${file}o"
	done
	echo "$special:$DEP" >> Makefile
	echo >> Makefile
done

for file in $(find . -iname "*.v")
do
	coqdep $MAPDIRS $file >> Makefile
	echo >> Makefile
	echo "${file}o:" >> Makefile
	echo "	$COQC $MAPDIRS $file" >> Makefile
	echo >> Makefile
done

echo "clean:" >> Makefile
for dir in $(find . -type d)
do
	echo "	rm -f $dir/*.{vo,glob,cmi,cmx,cmxs,native,aux,o}" >> Makefile
done
