Require Import Systems.F.Base.

Lemma map_et_exp_comm {V TV : Set} : forall e : exp V TV,
  forall (W TW : Set) (f : V -> W) (g : TV -> TW),
  map_exp_e f (map_exp_t g e) = map_exp_t g (map_exp_e f e).
Proof.
induction e; intros; simpl.
reflexivity.
rewrite IHe; reflexivity.
rewrite IHe1; rewrite IHe2; reflexivity.
rewrite IHe; reflexivity.
rewrite IHe; reflexivity.
Qed.

Lemma bind_map_id_typ {TV : Set} : forall T : typ TV,
  forall (TW : Set) (f : TW -> typ TV) (g : TV -> TW)
  (Hfg : forall a, f (g a) = typ_var a),
  bind_typ f (map_typ g T) = T.
Proof.
induction T; intros; simpl.
auto.
rewrite IHT1; try rewrite IHT2; auto.
rewrite IHT; try reflexivity.
  intro a; destruct a; simpl; trivial; rewrite Hfg; reflexivity.
Qed.

Lemma bind_map_id_t_exp {V TV : Set} : forall e : exp V TV,
  forall (TW : Set) (f : TW -> typ TV) (g : TV -> TW)
  (Hfg : forall a, f (g a) = typ_var a),
  bind_exp_t f (map_exp_t g e) = e.
Proof.
induction e; intros; simpl.
reflexivity.
rewrite IHe; try reflexivity; assumption.
rewrite IHe1; try rewrite IHe2; auto.
rewrite IHe; try reflexivity.
  intro a; destruct a; simpl; trivial; rewrite Hfg; reflexivity.
rewrite IHe; try rewrite bind_map_id_typ; auto.
Qed.

Lemma bind_map_id_e_exp {V TV : Set} : forall e : exp V TV,
  forall (W : Set) (f : W -> exp V TV) (g : V -> W)
  (Hfg : forall x, f (g x) = exp_var x),
  bind_exp_e f (map_exp_e g e) = e.
Proof.
induction e; intros; simpl.
auto.
rewrite IHe; try reflexivity.
  intro x; destruct x; simpl; trivial; rewrite Hfg; reflexivity.
rewrite IHe1; try rewrite IHe2; auto.
rewrite IHe; try reflexivity.
  intro x; unfold shift_et; rewrite Hfg; reflexivity.
rewrite IHe; auto.
Qed.