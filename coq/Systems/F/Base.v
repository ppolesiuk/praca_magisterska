Require Systems.F.Syntax.
Require Systems.F.SyntaxMeta.
Require Systems.F.Relations.

Include Systems.F.Syntax.
Include Systems.F.SyntaxMeta.
Include Systems.F.Relations.

Require Import Metatheory.TypeSystem.

Definition type_system (V TV : Set) : TypeSystem :=
  {| term  := exp V TV
   ; valid := well_typed
   ; red   := beta_red
  |}.

Axiom F_StrongNorm : forall V TV : Set, 
  TypeSystemStrongNorm (type_system V TV).
