Require Import Metatheory.VarSet.

Inductive typ (TV : Set) : Set :=
| typ_var   : TV -> typ TV
| typ_arrow : typ TV -> typ TV -> typ TV
| typ_all   : typ (inc TV) -> typ TV
.

Inductive exp (V TV : Set) : Set :=
| exp_var  : V -> exp V TV
| exp_abs  : exp (inc V) TV -> exp V TV
| exp_app  : exp V TV       -> exp V TV -> exp V TV
| exp_tabs : exp V (inc TV) -> exp V TV
| exp_tapp : exp V TV       -> typ TV   -> exp V TV
.

Arguments typ_var   [TV] _.
Arguments typ_arrow [TV] _ _.
Arguments typ_all   [TV] _.

Arguments exp_var  [V] [TV] _.
Arguments exp_abs  [V] [TV] _.
Arguments exp_app  [V] [TV] _ _.
Arguments exp_tabs [V] [TV] _.
Arguments exp_tapp [V] [TV] _ _.

Definition env (V TV : Set) := V -> typ TV.
