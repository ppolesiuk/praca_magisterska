Require Import Metatheory.VarSet.
Require Import Systems.F.Syntax.
Require Import Systems.F.SyntaxMeta.

Inductive typing {V TV : Set} (Gamma : env V TV) 
    : exp V TV -> typ TV -> Prop :=
| FT_Var  : forall x T,
    Gamma x = T ->
    typing Gamma (exp_var x) T
| FT_Abs  : forall e T1 T2,
    @typing _ _ (extend Gamma T1) e T2 ->
    typing Gamma (exp_abs e) (typ_arrow T1 T2)
| FT_App  : forall e1 e2 T1 T2,
    typing Gamma e1 (typ_arrow T2 T1) ->
    typing Gamma e2 T2 ->
    typing Gamma (exp_app e1 e2) T1
| FT_TAbs : forall e T,
    @typing _ _ (extend_t Gamma) e T ->
    typing Gamma (exp_tabs e) (typ_all T)
| FT_TApp : forall e T T' S,
    typing Gamma e (typ_all T) ->
    subst_typ T S = T' ->
    typing Gamma (exp_tapp e S) T'
.

Inductive beta {V TV : Set} : exp V TV -> exp V TV -> Prop :=
| Beta_E : forall e1 e2 es,
    subst_exp_e e1 e2 = es ->
    beta (exp_app (exp_abs e1) e2) es
| Beta_T : forall e T es,
    subst_exp_t e T = es ->
    beta (exp_tapp (exp_tabs e) T) es
.

Definition beta_red {V TV : Set} 
    : exp V TV -> exp V TV -> Prop := 
  context_cl (@beta).

Inductive well_typed {V TV : Set} (e : exp V TV) : Prop :=
| WellTyped : forall Gamma T, typing Gamma e T -> well_typed e
.
