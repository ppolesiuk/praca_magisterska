Require Import Metatheory.VarSet.
Require Import Metatheory.Relations.
Require Import Systems.F.Syntax.

Fixpoint map_typ {TV TW : Set} (f : TV -> TW) (T : typ TV) : typ TW :=
  match T with
  | typ_var a       => typ_var (f a)
  | typ_arrow T1 T2 => typ_arrow (map_typ f T1) (map_typ f T2)
  | typ_all T       => typ_all (map_typ (map_inc f) T)
  end.

Fixpoint map_exp_t {V TV TW : Set} (f : TV -> TW) (e : exp V TV)
    : exp V TW :=
  match e with
  | exp_var x     => exp_var x
  | exp_abs e     => exp_abs (map_exp_t f e)
  | exp_app e1 e2 => exp_app (map_exp_t f e1) (map_exp_t f e2)
  | exp_tabs e    => exp_tabs (map_exp_t (map_inc f) e)
  | exp_tapp e T  => exp_tapp (map_exp_t f e) (map_typ f T)
  end.

Fixpoint map_exp_e {V W TV : Set} (f : V -> W) (e : exp V TV) : exp W TV :=
  match e with
  | exp_var x     => exp_var (f x)
  | exp_abs e     => exp_abs (map_exp_e (map_inc f) e)
  | exp_app e1 e2 => exp_app (map_exp_e f e1) (map_exp_e f e2)
  | exp_tabs e    => exp_tabs (map_exp_e f e)
  | exp_tapp e T  => exp_tapp (map_exp_e f e) T
  end.

Definition shift_t {TV TW : Set} (f : TV -> typ TW) (a : inc TV) 
    : typ (inc TW) :=
  match a with
  | VZ   => typ_var VZ
  | VS a => map_typ (@VS _) (f a)
  end.

Definition shift_et {V W TV : Set} (f : V -> exp W TV) (x : V)
    : exp W (inc TV) :=
  map_exp_t (@VS _) (f x).

Definition shift_e {V W TV : Set} (f : V -> exp W TV) (x : inc V) 
    : exp (inc W) TV :=
  match x with
  | VZ   => exp_var VZ
  | VS x => map_exp_e (@VS _) (f x)
  end.

Fixpoint bind_typ {TV TW : Set} (f : TV -> typ TW) (T : typ TV) : typ TW :=
  match T with
  | typ_var a       => f a
  | typ_arrow T1 T2 => typ_arrow (bind_typ f T1) (bind_typ f T2)
  | typ_all T       => typ_all (bind_typ (shift_t f) T)
  end.

Fixpoint bind_exp_t {V TV TW : Set} (f : TV -> typ TW) (e : exp V TV)
    : exp V TW :=
  match e with
  | exp_var x     => exp_var x
  | exp_abs e     => exp_abs (bind_exp_t f e)
  | exp_app e1 e2 => exp_app (bind_exp_t f e1) (bind_exp_t f e2)
  | exp_tabs e    => exp_tabs (bind_exp_t (shift_t f) e)
  | exp_tapp e T  => exp_tapp (bind_exp_t f e) (bind_typ f T)
  end.

Fixpoint bind_exp_e {V W TV : Set} (f : V -> exp W TV) (e : exp V TV) 
    : exp W TV :=
  match e with
  | exp_var x     => f x
  | exp_abs e     => exp_abs (bind_exp_e (shift_e f) e)
  | exp_app e1 e2 => exp_app (bind_exp_e f e1) (bind_exp_e f e2)
  | exp_tabs e    => exp_tabs (bind_exp_e (shift_et f) e)
  | exp_tapp e T  => exp_tapp (bind_exp_e f e) T
  end.

Definition subst_start_typ {TV : Set} (T : typ TV) (a : inc TV)
    : typ TV :=
  match a with
  | VZ   => T
  | VS a => typ_var a
  end.

Definition subst_start_exp {V TV : Set} (e : exp V TV) (x : inc V)
    : exp V TV :=
  match x with
  | VZ   => e
  | VS x => exp_var x
  end.

Definition subst_typ {TV : Set} (T : typ (inc TV)) (S : typ TV) 
    : typ TV :=
  bind_typ (subst_start_typ S) T.

Definition subst_exp_t {V TV : Set} (e : exp V (inc TV)) (T : typ TV)
    : exp V TV :=
  bind_exp_t (subst_start_typ T) e.

Definition subst_exp_e {V TV : Set} (e : exp (inc V) TV) (es : exp V TV) 
    : exp V TV :=
  bind_exp_e (subst_start_exp es) e.

Section ContextCl.

Variable R : forall (V TV : Set), exp V TV -> exp V TV -> Prop.

Inductive context_cl {V TV : Set} : exp V TV -> exp V TV -> Prop :=
| CCL_Step : forall e e',
    R _ _ e e' ->
    context_cl e e'
| CCL_Abs  : forall e e',
    @context_cl _ _ e e' ->
    context_cl (exp_abs e) (exp_abs e')
| CCL_App1 : forall e1 e1' e2,
    context_cl e1 e1' ->
    context_cl (exp_app e1 e2) (exp_app e1' e2)
| CCL_App2 : forall e1 e2 e2',
    context_cl e2 e2' ->
    context_cl (exp_app e1 e2) (exp_app e1 e2')
| CCL_TAbs : forall e e',
    @context_cl _ _ e e' ->
    context_cl (exp_tabs e) (exp_tabs e')
| CCL_TApp : forall e e' T,
    context_cl e e' ->
    context_cl (exp_tapp e T) (exp_tapp e' T)
.

End ContextCl.

Lemma ccl_plus_ccl_lemma
  (R : forall V TV : Set, exp V TV -> exp V TV -> Prop)
  {V TV : Set} : forall e1 e2 : exp V TV,
  context_cl (fun V TV => (@context_cl R V TV)^+) e1 e2 -> 
    (context_cl R)^+ e1 e2.
Proof.
intros e1 e2 Hred; induction Hred; try assumption; clear Hred.
induction IHHred.
  constructor 1; apply CCL_Abs; assumption.
  econstructor 2; [ apply CCL_Abs | ]; eassumption.
induction IHHred.
  constructor 1; apply CCL_App1; assumption.
  econstructor 2; [ apply CCL_App1 | ]; eassumption.
induction IHHred.
  constructor 1; apply CCL_App2; assumption.
  econstructor 2; [ apply CCL_App2 | ]; eassumption.
induction IHHred.
  constructor 1; apply CCL_TAbs; assumption.
  econstructor 2; [ apply CCL_TAbs | ]; eassumption.
induction IHHred.
  constructor 1; apply CCL_TApp; assumption.
  econstructor 2; [ apply CCL_TApp | ]; eassumption.
Qed.

Definition extend {V TV : Set} (Gamma : env V TV) (T : typ TV)
    : env (inc V) TV :=
  fun (x : inc V) =>
  match x with
  | VZ   => T
  | VS x => Gamma x
  end.

Definition extend_t {V TV : Set} (Gamma : env V TV) : env V (inc TV) :=
  fun (x : V) => map_typ (@VS _) (Gamma x).
