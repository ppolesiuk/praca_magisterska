Require Import Metatheory.VarSet.
Require Import Systems.FboundedCommon.Syntax.

Fixpoint map_typ {TV TW : Set} (f : TV -> TW) (T : typ TV) : typ TW :=
  match T with
  | typ_top         => typ_top
  | typ_var a       => typ_var (f a)
  | typ_arrow T1 T2 => typ_arrow (map_typ f T1) (map_typ f T2)
  | typ_all T1 T2   => 
      typ_all (map_typ (map_inc f) T1) (map_typ (map_inc f) T2)
  end.

Definition shift_t {TV TW : Set} (f : TV -> typ TW) (a : inc TV) 
    : typ (inc TW) :=
  match a with
  | VZ   => typ_var VZ
  | VS a => map_typ (@VS _) (f a)
  end.

Fixpoint bind_typ {TV TW : Set} (f : TV -> typ TW) (T : typ TV) : typ TW :=
  match T with
  | typ_top         => typ_top
  | typ_var a       => f a
  | typ_arrow T1 T2 => typ_arrow (bind_typ f T1) (bind_typ f T2)
  | typ_all T1 T2   => 
      typ_all (bind_typ (shift_t f) T1) (bind_typ (shift_t f) T2)
  end.

Definition subst_start_typ {TV : Set} (T : typ TV) (a : inc TV)
    : typ TV :=
  match a with
  | VZ   => T
  | VS a => typ_var a
  end.

Definition subst_typ {TV : Set} (T : typ (inc TV)) (S : typ TV) 
    : typ TV :=
  bind_typ (subst_start_typ S) T.

Definition extend_eenv {V TV : Set} (Gamma : eenv V TV) (T : typ TV)
    : eenv (inc V) TV :=
  fun (x : inc V) =>
  match x with
  | VZ   => T
  | VS x => Gamma x
  end.

Definition extend_tenv {TV : Set} (Gamma : tenv TV) (T : typ (inc TV)) 
    : tenv (inc TV) :=
  fun (a : inc TV) =>
  match a with
  | VZ   => T
  | VS a => map_typ (@VS _) (Gamma a)
  end.

Definition shift_eenv {V TV : Set} (Gamma : eenv V TV) : eenv V (inc TV) :=
  fun (x : V) => map_typ (@VS _) (Gamma x).

Definition extend_e {V TV : Set} (Gamma : env V TV) (T : typ TV)
    : env (inc V) TV :=
  {| env_e := extend_eenv (env_e Gamma) T
   ; env_t := env_t Gamma
  |}.

Definition extend_t {V TV : Set} (Gamma : env V TV) (T : typ (inc TV))
    : env V (inc TV) :=
  {| env_e := shift_eenv (env_e Gamma)
   ; env_t := extend_tenv (env_t Gamma) T
  |}.
