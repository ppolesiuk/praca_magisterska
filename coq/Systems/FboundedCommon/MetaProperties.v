Require Import Metatheory.VarSet.
Require Import Systems.FboundedCommon.Base.

Lemma map_typ_ext {TV : Set} :
  forall (T : typ TV) (TW : Set) (f g : TV -> TW),
  (forall a, f a = g a) -> map_typ f T = map_typ g T.
Proof.
induction T; simpl; intros TW f g fg_ext; simpl.
reflexivity.
rewrite fg_ext; reflexivity.
erewrite IHT1; [| eassumption ]; erewrite IHT2; trivial.
erewrite IHT1.
  erewrite IHT2.
    reflexivity.
  intro a; destruct a; simpl; trivial; rewrite fg_ext; reflexivity.
  intro a; destruct a; simpl; trivial; rewrite fg_ext; reflexivity.
Qed.

Lemma bind_typ_ext {TV : Set} :
  forall (T : typ TV) (TW : Set) (f g : TV -> typ TW),
  (forall a, f a = g a) -> bind_typ f T = bind_typ g T.
Proof.
induction T; simpl; intros TW f g fg_ext.
reflexivity.
apply fg_ext.
erewrite IHT1; [| eassumption ]; erewrite IHT2; trivial.
erewrite IHT1.
  erewrite IHT2.
  reflexivity.
intro a; destruct a; simpl; trivial.
  rewrite fg_ext; reflexivity.
intro a; destruct a; simpl; trivial.
  rewrite fg_ext; reflexivity.
Qed.

Lemma map_map_typ {TV : Set} :
  forall (T : typ TV) (TW TX : Set) (f : TV -> TW) (g : TW -> TX),
    map_typ g (map_typ f T) = map_typ (fun x => g (f x)) T.
Proof.
induction T; simpl; intros TW TX f g.
reflexivity.
reflexivity.
rewrite IHT1; rewrite IHT2; reflexivity.
rewrite IHT1; rewrite IHT2.
  erewrite (map_typ_ext T1).
  erewrite (map_typ_ext T2).
    reflexivity.
  intro a; destruct a; reflexivity.
  intro a; destruct a; reflexivity.
Qed.

Lemma bind_map_typ {TV : Set} :
  forall (T : typ TV) (TW TX : Set) (f : TV -> TW) (g : TW -> typ TX),
    bind_typ g (map_typ f T) = bind_typ (fun x => g (f x)) T.
Proof.
induction T; simpl; intros TW TX f g.
reflexivity.
reflexivity.
rewrite IHT1; rewrite IHT2; reflexivity.
rewrite IHT1; rewrite IHT2.
  erewrite (bind_typ_ext T1).
  erewrite (bind_typ_ext T2).
    reflexivity.
  intro a; destruct a; trivial.
  intro a; destruct a; trivial.
Qed.

Lemma map_bind_typ {TV : Set} :
  forall (T : typ TV) (TW TX : Set) (f : TV -> typ TW) (g : TW -> TX),
    map_typ g (bind_typ f T) = bind_typ (fun x => map_typ g (f x)) T.
Proof.
induction T; simpl; intros TW TX f g.
reflexivity.
reflexivity.
rewrite IHT1; rewrite IHT2; reflexivity.
rewrite IHT1; rewrite IHT2.
  erewrite (bind_typ_ext T1).
  erewrite (bind_typ_ext T2).
    reflexivity.
  intro a; destruct a; simpl; trivial.
    repeat rewrite map_map_typ; reflexivity.
  intro a; destruct a; simpl; trivial.
    repeat rewrite map_map_typ; reflexivity.
Qed.

Lemma bind_shift_map_typ {TV TW : Set} :
  forall (T : typ TV) (f : TV -> typ TW),
  bind_typ (shift_t f) (map_typ (@VS _) T) =
    map_typ (@VS _) (bind_typ f T).
Proof.
intros.
rewrite bind_map_typ; simpl; rewrite <- map_bind_typ; reflexivity.
Qed.

Lemma bind_bind_typ {TV : Set} :
  forall (T : typ TV) (TW TX : Set) (f : TV -> typ TW) (g : TW -> typ TX),
    bind_typ g (bind_typ f T) = 
      bind_typ (fun x => bind_typ g (f x)) T.
Proof.
induction T; intros; simpl.
reflexivity.
reflexivity.
rewrite IHT1; rewrite IHT2; reflexivity.
rewrite IHT1; rewrite IHT2.
  erewrite (bind_typ_ext T1).
  erewrite (bind_typ_ext T2).
    reflexivity.
  intro a; destruct a; simpl; trivial; apply bind_shift_map_typ.
  intro a; destruct a; simpl; trivial; apply bind_shift_map_typ.
Qed.

Lemma bind_return_typ {TV : Set} :
  forall (T : typ TV), bind_typ (@typ_var TV) T = T.
Proof.
induction T; simpl.
reflexivity.
reflexivity.
rewrite IHT1; rewrite IHT2; reflexivity.
rewrite (bind_typ_ext T1 _ _ (@typ_var _)).
rewrite (bind_typ_ext T2 _ _ (@typ_var _)).
  rewrite IHT1; rewrite IHT2; reflexivity.
intro a; destruct a; reflexivity.
intro a; destruct a; reflexivity.
Qed.

Lemma bind_map_id_typ {TV : Set} : forall T : typ TV,
  forall (TW : Set) (f : TW -> typ TV) (g : TV -> TW)
  (Hfg : forall a, f (g a) = typ_var a),
  bind_typ f (map_typ g T) = T.
Proof.
induction T; intros; simpl.
reflexivity.
auto.
rewrite IHT1; try rewrite IHT2; auto.
rewrite IHT1; try rewrite IHT2; try reflexivity.
  intro a; destruct a; simpl; try rewrite Hfg; reflexivity.
  intro a; destruct a; simpl; try rewrite Hfg; reflexivity.
Qed.

Lemma bind_subst_typ_comm {TV TW : Set} :
  forall (T : typ (inc TV)) (S : typ TV) (f : TV -> typ TW),
  subst_typ (bind_typ (shift_t f) T) (bind_typ f S) =
    bind_typ f (subst_typ T S).
Proof.
intros; unfold subst_typ; repeat rewrite bind_bind_typ.
apply bind_typ_ext.
intro a; destruct a; simpl.
  reflexivity.
apply bind_map_id_typ; reflexivity.
Qed.

Lemma map_subst_typ_comm {TV TW : Set} :
  forall (T : typ (inc TV)) (S : typ TV) (f : TV -> TW),
  subst_typ (map_typ (map_inc f) T) (map_typ f S) =
    map_typ f (subst_typ T S).
Proof.
intros; unfold subst_typ.
rewrite bind_map_typ; rewrite map_bind_typ.
apply bind_typ_ext.
intro a; destruct a; reflexivity.
Qed.