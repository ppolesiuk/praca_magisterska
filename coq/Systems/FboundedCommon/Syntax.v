Require Import Metatheory.VarSet.

Inductive typ (TV : Set) : Set :=
| typ_top   : typ TV
| typ_var   : TV -> typ TV
| typ_arrow : typ TV -> typ TV -> typ TV
| typ_all   : typ (inc TV) -> typ (inc TV) -> typ TV
.

Arguments typ_top   [TV].
Arguments typ_var   [TV] _.
Arguments typ_arrow [TV] _ _.
Arguments typ_all   [TV] _ _.

Definition eenv (V TV : Set) := V -> typ TV.
Definition tenv (TV : Set) := TV -> typ TV.

Record env (V TV : Set) : Set :=
  { env_e : eenv V TV
  ; env_t : tenv TV
  }.

Arguments env_e [V] [TV] _ _.
Arguments env_t [V] [TV] _ _.