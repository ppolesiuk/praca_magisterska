Require Import Metatheory.VarSet.
Require Import Systems.Fbounded.Syntax.
Require Import Systems.FboundedCommon.SyntaxMeta.

Fixpoint map_exp_t {V TV TW : Set} (f : TV -> TW) (e : exp V TV)
    : exp V TW :=
  match e with
  | exp_var x     => exp_var x
  | exp_abs T e   => exp_abs (map_typ f T) (map_exp_t f e)
  | exp_app e1 e2 => exp_app (map_exp_t f e1) (map_exp_t f e2)
  | exp_tabs U e  => 
      exp_tabs (map_typ (map_inc f) U) (map_exp_t (map_inc f) e)
  | exp_tapp e T  => exp_tapp (map_exp_t f e) (map_typ f T)
  end.

Fixpoint map_exp_e {V W TV : Set} (f : V -> W) (e : exp V TV) : exp W TV :=
  match e with
  | exp_var x     => exp_var (f x)
  | exp_abs T e   => exp_abs T (map_exp_e (map_inc f) e)
  | exp_app e1 e2 => exp_app (map_exp_e f e1) (map_exp_e f e2)
  | exp_tabs U e  => exp_tabs U (map_exp_e f e)
  | exp_tapp e T  => exp_tapp (map_exp_e f e) T
  end.

Definition shift_et {V W TV : Set} (f : V -> exp W TV) (x : V)
    : exp W (inc TV) :=
  map_exp_t (@VS _) (f x).

Definition shift_e {V W TV : Set} (f : V -> exp W TV) (x : inc V) 
    : exp (inc W) TV :=
  match x with
  | VZ   => exp_var VZ
  | VS x => map_exp_e (@VS _) (f x)
  end.

Fixpoint bind_exp_t {V TV TW : Set} (f : TV -> typ TW) (e : exp V TV)
    : exp V TW :=
  match e with
  | exp_var x     => exp_var x
  | exp_abs T e   => exp_abs (bind_typ f T) (bind_exp_t f e)
  | exp_app e1 e2 => exp_app (bind_exp_t f e1) (bind_exp_t f e2)
  | exp_tabs U e  => 
      exp_tabs (bind_typ (shift_t f) U) (bind_exp_t (shift_t f) e)
  | exp_tapp e T  => exp_tapp (bind_exp_t f e) (bind_typ f T)
  end.

Fixpoint bind_exp_e {V W TV : Set} (f : V -> exp W TV) (e : exp V TV) 
    : exp W TV :=
  match e with
  | exp_var x     => f x
  | exp_abs T e   => exp_abs T (bind_exp_e (shift_e f) e)
  | exp_app e1 e2 => exp_app (bind_exp_e f e1) (bind_exp_e f e2)
  | exp_tabs U e  => exp_tabs U (bind_exp_e (shift_et f) e)
  | exp_tapp e T  => exp_tapp (bind_exp_e f e) T
  end.

Definition subst_start_exp {V TV : Set} (e : exp V TV) (x : inc V)
    : exp V TV :=
  match x with
  | VZ   => e
  | VS x => exp_var x
  end.

Definition subst_exp_t {V TV : Set} (e : exp V (inc TV)) (T : typ TV)
    : exp V TV :=
  bind_exp_t (subst_start_typ T) e.

Definition subst_exp_e {V TV : Set} (e : exp (inc V) TV) (es : exp V TV) 
    : exp V TV :=
  bind_exp_e (subst_start_exp es) e.

Section ContextCl.

Variable R : forall (V TV : Set), exp V TV -> exp V TV -> Prop.

Inductive context_cl {V TV : Set} : exp V TV -> exp V TV -> Prop :=
| CCL_Step : forall e e',
    R _ _ e e' ->
    context_cl e e'
| CCL_Abs  : forall T e e',
    @context_cl _ _ e e' ->
    context_cl (exp_abs T e) (exp_abs T e')
| CCL_App1 : forall e1 e1' e2,
    context_cl e1 e1' ->
    context_cl (exp_app e1 e2) (exp_app e1' e2)
| CCL_App2 : forall e1 e2 e2',
    context_cl e2 e2' ->
    context_cl (exp_app e1 e2) (exp_app e1 e2')
| CCL_TAbs : forall U e e',
    @context_cl _ _ e e' ->
    context_cl (exp_tabs U e) (exp_tabs U e')
| CCL_TApp : forall e e' T,
    context_cl e e' ->
    context_cl (exp_tapp e T) (exp_tapp e' T)
.

End ContextCl.
