Require Import Metatheory.VarSet.
Require Export Systems.FboundedCommon.Syntax.

Inductive exp (V TV : Set) : Set :=
| exp_var  : V -> exp V TV
| exp_abs  : typ TV       -> exp (inc V) TV -> exp V TV
| exp_app  : exp V TV     -> exp V TV       -> exp V TV
| exp_tabs : typ (inc TV) -> exp V (inc TV) -> exp V TV
| exp_tapp : exp V TV     -> typ TV         -> exp V TV
.

Arguments exp_var  [V] [TV] _.
Arguments exp_abs  [V] [TV] _ _.
Arguments exp_app  [V] [TV] _ _.
Arguments exp_tabs [V] [TV] _ _.
Arguments exp_tapp [V] [TV] _ _.
