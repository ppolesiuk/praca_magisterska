Require Systems.FboundedCommon.Base.
Require Systems.Fbounded.Syntax.
Require Systems.Fbounded.SyntaxMeta.
Require Systems.Fbounded.Relations.

Include Systems.FboundedCommon.Base.
Include Systems.Fbounded.Syntax.
Include Systems.Fbounded.SyntaxMeta.
Include Systems.Fbounded.Relations.

Require Import Metatheory.TypeSystem.

Definition type_system (V TV : Set) : TypeSystem :=
  {| term  := exp V TV
   ; valid := well_typed
   ; red   := beta_red
  |}.