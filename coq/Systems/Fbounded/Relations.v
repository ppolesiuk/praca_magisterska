Require Import Metatheory.VarSet.
Require Import Systems.FboundedCommon.Base.
Require Import Systems.Fbounded.Syntax.
Require Import Systems.Fbounded.SyntaxMeta.

Inductive sub {TV : Set} (Gamma : tenv TV) : typ TV -> typ TV -> Prop :=
| S_Refl  : forall T,
    sub Gamma T T
| S_Trans : forall T1 T2 T3,
    sub Gamma T2 T3 ->
    sub Gamma T1 T2 ->
    sub Gamma T1 T3
| S_Top   : forall T,
    sub Gamma T typ_top
| S_Var   : forall a T,
    Gamma a = T ->
    sub Gamma (typ_var a) T
| S_Arrow : forall S1 S2 T1 T2,
    sub Gamma S2 S1 ->
    sub Gamma T1 T2 ->
    sub Gamma (typ_arrow S1 T1) (typ_arrow S2 T2)
| S_All   : forall U1 U2 T1 T2,
    @sub _ (extend_tenv Gamma U2) (typ_var VZ) U1 ->
    @sub _ (extend_tenv Gamma U2) T1 T2 ->
    sub Gamma (typ_all U1 T1) (typ_all U2 T2)
.

Inductive typing {V TV : Set} (Gamma : env V TV) 
    : exp V TV -> typ TV -> Prop :=
| T_Var  : forall x T,
    env_e Gamma x = T ->
    typing Gamma (exp_var x) T
| T_Abs  : forall e T1 T2,
    @typing _ _ (extend_e Gamma T1) e T2 ->
    typing Gamma (exp_abs T1 e) (typ_arrow T1 T2)
| T_App  : forall e1 e2 T1 T2,
    typing Gamma e1 (typ_arrow T2 T1) ->
    typing Gamma e2 T2 ->
    typing Gamma (exp_app e1 e2) T1
| T_TAbs : forall e U T,
    @typing _ _ (extend_t Gamma U) e T ->
    typing Gamma (exp_tabs U e) (typ_all U T)
| T_TApp : forall e U T T' S,
    typing Gamma e (typ_all U T) ->
    sub (env_t Gamma) S (subst_typ U S) ->
    subst_typ T S = T' ->
    typing Gamma (exp_tapp e S) T'
| T_Sub  : forall e S T,
    typing Gamma e S ->
    sub (env_t Gamma) S T ->
    typing Gamma e T
.

Inductive beta {V TV : Set} : exp V TV -> exp V TV -> Prop :=
| Beta_E : forall T e1 e2 es,
    subst_exp_e e1 e2 = es ->
    beta (exp_app (exp_abs T e1) e2) es
| Beta_T : forall U e T es,
    subst_exp_t e T = es ->
    beta (exp_tapp (exp_tabs U e) T) es
.

Definition beta_red {V TV : Set} 
    : exp V TV -> exp V TV -> Prop := 
  context_cl (@beta).

Inductive well_typed {V TV : Set} (e : exp V TV) : Prop :=
| WellTyped : forall Gamma T, typing Gamma e T -> well_typed e
.
