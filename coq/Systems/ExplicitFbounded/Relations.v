Require Import Metatheory.VarSet.
Require Import Metatheory.Relations.
Require Import Systems.FboundedCommon.Base.
Require Import Systems.ExplicitFbounded.Syntax.
Require Import Systems.ExplicitFbounded.SyntaxMeta.

Inductive sub {TV : Set} (Gamma : tenv TV) 
    : crc TV -> typ TV -> typ TV -> Prop :=
| ES_Refl  : forall T,
    sub Gamma crc_id T T
| ES_Trans : forall T1 T2 T3 c1 c2,
    sub Gamma c1 T2 T3 ->
    sub Gamma c2 T1 T2 ->
    sub Gamma (crc_comp c1 c2) T1 T3
| ES_Top   : forall T,
    sub Gamma crc_top T typ_top
| ES_Var   : forall a T,
    Gamma a = T ->
    sub Gamma (crc_var a) (typ_var a) T
| ES_Arrow : forall S1 S2 T1 T2 c1 c2,
    sub Gamma c1 S2 S1 ->
    sub Gamma c2 T1 T2 ->
    sub Gamma (crc_arrow c1 c2) (typ_arrow S1 T1) (typ_arrow S2 T2)
| ES_All   : forall U1 U2 T1 T2 c1 c2,
    @sub _ (extend_tenv Gamma U2) c1 (typ_var VZ) U1 ->
    @sub _ (extend_tenv Gamma U2) c2 T1 T2 ->
    sub Gamma (crc_all c1 c2) (typ_all U1 T1) (typ_all U2 T2)
.

Inductive typing {V TV : Set} (Gamma : env V TV) 
    : exp V TV -> typ TV -> Prop :=
| T_Var  : forall x T,
    env_e Gamma x = T ->
    typing Gamma (exp_var x) T
| T_Abs  : forall e T1 T2,
    @typing _ _ (extend_e Gamma T1) e T2 ->
    typing Gamma (exp_abs T1 e) (typ_arrow T1 T2)
| T_App  : forall e1 e2 T1 T2,
    typing Gamma e1 (typ_arrow T2 T1) ->
    typing Gamma e2 T2 ->
    typing Gamma (exp_app e1 e2) T1
| T_TAbs : forall e U T,
    @typing _ _ (extend_t Gamma U) e T ->
    typing Gamma (exp_tabs U e) (typ_all U T)
| T_TApp : forall e U T T' S c,
    typing Gamma e (typ_all U T) ->
    sub (env_t Gamma) c S (subst_typ U S) ->
    subst_typ T S = T' ->
    typing Gamma (exp_tapp e S c) T'
| T_Sub  : forall e c S T,
    typing Gamma e S ->
    sub (env_t Gamma) c S T ->
    typing Gamma (exp_capp c e) T
.

Inductive beta {V TV : Set} : exp V TV -> exp V TV -> Prop :=
| Beta_E : forall T e1 e2 es,
    subst_exp_e e1 e2 = es ->
    beta (exp_app (exp_abs T e1) e2) es
| Beta_T : forall U e T c es,
    subst_exp_t e T c = es ->
    beta (exp_tapp (exp_tabs U e) T c) es
.

Inductive iota {V TV : Set} : exp V TV -> exp V TV -> Prop :=
| Iota_Id    : forall e,
    iota (exp_capp crc_id e) e
| Iota_Comp  : forall c1 c2 e,
    iota (exp_capp (crc_comp c1 c2) e) (exp_capp c1 (exp_capp c2 e))
| Iota_Arrow : forall c1 c2 e1 e2,
    iota (exp_app (exp_capp (crc_arrow c1 c2) e1) e2)
      (exp_capp c2 (exp_app e1 (exp_capp c1 e2)))
| Iota_All   : forall c1 c2 c c1' c2' e T,
    subst_crc c1 c = c1' ->
    subst_crc c2 c = c2' ->
    iota (exp_tapp (exp_capp (crc_all c1 c2) e) T c)
      (exp_capp c2' (exp_tapp e T c1'))
.

Definition beta_red {V TV : Set} 
    : exp V TV -> exp V TV -> Prop := 
  context_cl (@beta).

Definition iota_red {V TV : Set} 
    : exp V TV -> exp V TV -> Prop := 
  context_cl (@iota).

Definition beta_iota_red {V TV : Set} 
    : exp V TV -> exp V TV -> Prop :=
  union beta_red iota_red.

Inductive well_typed {V TV : Set} (e : exp V TV) : Prop :=
| WellTyped : forall Gamma T, typing Gamma e T -> well_typed e
.
