Require Import Metatheory.Misc.
Require Import Metatheory.VarSet.
Require Import Systems.FboundedCommon.MetaProperties.
Require Import Systems.ExplicitFbounded.Base.

Lemma sub_ext {TV : Set} :
  forall (Gamma : tenv TV) c T1 T2, sub Gamma c T1 T2 ->
  forall Sigma (Hext : forall a, Sigma a = Gamma a),
  sub Sigma c T1 T2.
Proof.
induction 1; intros; simpl.
constructor.
econstructor; auto.
constructor.
constructor; subst; auto.
constructor; auto.
constructor.
  apply IHsub1; intro a; destruct a; simpl; trivial; rewrite Hext; reflexivity.
  apply IHsub2; intro a; destruct a; simpl; trivial; rewrite Hext; reflexivity.
Qed.

Lemma map_crc_sub {TV : Set} :
  forall (Gamma : tenv TV) c T1 T2, sub Gamma c T1 T2 ->
  forall (TW : Set) (Sigma : tenv TW) (f : TV -> TW)
    (Hf : forall a, Sigma (f a) = map_typ f (Gamma a)),
  sub Sigma (map_crc f c) (map_typ f T1) (map_typ f T2).
Proof.
induction 1; intros; simpl.
constructor.
econstructor; auto.
constructor.
constructor; subst; auto.
constructor; auto.
constructor.
  change (typ_var VZ) with (map_typ (map_inc f) (typ_var VZ)).
    apply IHsub1.
    intro a; destruct a; simpl; trivial; rewrite Hf.
      repeat rewrite map_map_typ; reflexivity.
  apply IHsub2.
    intro a; destruct a; simpl; trivial; rewrite Hf.
      repeat rewrite map_map_typ; reflexivity.
Qed.

Lemma map_exp_t_typing {V TV : Set} :
  forall (Gamma : env V TV) e T, typing Gamma e T ->
  forall (TW : Set) (Sigma : env V TW) (f : TV -> TW)
    (He : forall x, env_e Sigma x = map_typ f (env_e Gamma x))
    (Ht : forall a, env_t Sigma (f a) = map_typ f (env_t Gamma a)),
  typing Sigma (map_exp_t f e) (map_typ f T).
Proof.
induction 1; intros; simpl.
constructor; subst; auto.
constructor; apply IHtyping; auto.
  intro x; destruct x; simpl; auto.
econstructor; eauto.
constructor; apply IHtyping; simpl.
  intro x; unfold shift_eenv; simpl; rewrite He.
    repeat rewrite map_map_typ; reflexivity.
  intro a; destruct a; simpl; trivial; rewrite Ht.
    repeat rewrite map_map_typ; reflexivity.
econstructor; simpl; eauto; fold (@map_typ).
  rewrite map_subst_typ_comm; eapply map_crc_sub; eauto.
  rewrite map_subst_typ_comm; subst; reflexivity.
econstructor; eauto.
  eapply map_crc_sub; eauto.
Qed.

Lemma map_exp_e_typing {V TV : Set} :
  forall (Gamma : env V TV) e T, typing Gamma e T ->
  forall (W : Set) (Sigma : env W TV) (f : V -> W)
    (He : forall x, env_e Sigma (f x) = env_e Gamma x)
    (Ht : forall a, env_t Sigma a = env_t Gamma a),
  typing Sigma (map_exp_e f e) T.
Proof.
induction 1; intros; simpl.
constructor; subst; auto.
constructor; apply IHtyping.
  intro x; destruct x; simpl; auto.
  apply Ht.
econstructor; auto.
constructor; apply IHtyping; simpl.
  intro x; unfold shift_eenv; rewrite He; reflexivity.
  intro a; destruct a; simpl; trivial; rewrite Ht; reflexivity.
econstructor; eauto.
  eapply sub_ext; eauto.
econstructor; eauto.
  eapply sub_ext; eauto.
Qed.

Lemma bind_crc_sub {TV : Set} :
  forall (Gamma : tenv TV) c T1 T2, sub Gamma c T1 T2 ->
    forall (TW : Set) (Sigma : tenv TW)
      (fc : TV -> crc TW) (ft : TV -> typ TW)
      (Hf : forall a, sub Sigma (fc a) (ft a) (bind_typ ft (Gamma a))),
  sub Sigma (bind_crc fc c) (bind_typ ft T1) (bind_typ ft T2).
Proof.
induction 1; intros; simpl.
constructor.
econstructor.
  apply IHsub1; assumption.
  apply IHsub2; assumption.
constructor.
subst; auto.
constructor.
  apply IHsub1; assumption.
  apply IHsub2; assumption.
constructor.
  change (typ_var VZ) with (bind_typ (shift_t ft) (typ_var VZ));
    apply IHsub1.
    intro a; destruct a; simpl.
      constructor; reflexivity.
      rewrite bind_shift_map_typ.
      eapply map_crc_sub; auto.
  apply IHsub2.
    intro a; destruct a; simpl.
      constructor; reflexivity.
      rewrite bind_shift_map_typ.
      eapply map_crc_sub; auto.
Qed.

Lemma bind_exp_t_typing {V TV : Set} :
  forall Gamma (e : exp V TV) T, typing Gamma e T ->
  forall (TW : Set) Sigma
    (f : TV -> typ TW * crc TW) (g : TV -> typ TW)
    (Hfg  : forall a, g a = fst (f a))
    (He   : forall x, env_e Sigma x = bind_typ g (env_e Gamma x))
    (Ht   : forall a, 
      sub (env_t Sigma) (snd (f a)) (g a) (bind_typ g (env_t Gamma a))),
  typing Sigma (bind_exp_t f e) (bind_typ g T).
Proof.
induction 1; intros; simpl.
constructor; subst; auto.
erewrite (bind_typ_ext T1).
  constructor; apply IHtyping; simpl; auto.
  intro x; destruct x; simpl; auto.
  symmetry; apply Hfg.
econstructor.
  apply IHtyping1; simpl; auto.
  apply IHtyping2; simpl; auto.
erewrite (bind_typ_ext U).
  constructor; apply IHtyping; simpl; auto.
  intro a; destruct a; simpl; trivial; rewrite Hfg; reflexivity.
  intro x; unfold shift_eenv.
    rewrite bind_shift_map_typ; rewrite He; reflexivity.
  intro a; destruct a; simpl.
    constructor; reflexivity.
    rewrite bind_shift_map_typ.
    eapply map_crc_sub; eauto.
  intro a; destruct a; simpl; auto.
    rewrite Hfg; reflexivity.
econstructor.
  apply IHtyping; simpl; eauto.
  fold (@bind_typ).
    erewrite (bind_typ_ext U).
      rewrite bind_subst_typ_comm.
      eapply bind_crc_sub; try eassumption.
      intro a; erewrite bind_typ_ext.
        unfold f_fst; rewrite <- Hfg; apply Ht.
      symmetry; eauto.
    intro a; destruct a; simpl; try rewrite Hfg; reflexivity.
  fold (@bind_typ).
    subst; rewrite <- bind_subst_typ_comm.
    erewrite (bind_typ_ext S); try reflexivity.
    intro; rewrite Hfg; auto.
econstructor.
  apply IHtyping; simpl; eauto.
  eapply bind_crc_sub; eauto.
Qed.

Lemma bind_exp_e_typing {V TV : Set} :
  forall Gamma (e : exp V TV) T, typing Gamma e T ->
  forall (W : Set) Sigma (f : V -> exp W TV)
    (He : forall x, typing Sigma (f x) (env_e Gamma x))
    (Ht : forall a, env_t Sigma a = env_t Gamma a),
  typing Sigma (bind_exp_e f e) T.
Proof.
induction 1; intros; simpl.
subst; auto.
constructor; apply IHtyping; auto.
  intro x; destruct x; simpl.
    constructor; reflexivity.
    eapply map_exp_e_typing; auto.
econstructor; eauto.
constructor; apply IHtyping; simpl.
  unfold shift_et; unfold shift_eenv.
    intro x; eapply map_exp_t_typing; eauto.
  intro x; destruct x; simpl; try rewrite Ht; reflexivity.
econstructor; eauto.
  eapply sub_ext; eauto.
econstructor; eauto.
  eapply sub_ext; eauto.
Qed.

Lemma subst_crc_sub {TV : Set} :
  forall Gamma c1 c2 T1 T2 U (S : typ TV),
  sub (extend_tenv Gamma U) c1 T1 T2 -> 
  sub Gamma c2 S (subst_typ U S) ->
  sub Gamma (subst_crc c1 c2) (subst_typ T1 S) (subst_typ T2 S).
Proof.
intros.
unfold subst_crc; unfold subst_typ.
eapply bind_crc_sub.
  eassumption.
intro a; destruct a; simpl.
  assumption.
constructor; rewrite bind_map_id_typ; reflexivity.
Qed.

Lemma subst_exp_t_typing {V TV : Set} :
  forall (Gamma : env V TV) e c T U S,
    typing (extend_t Gamma U) e T ->
    sub (env_t Gamma) c S (subst_typ U S) ->
    typing Gamma (subst_exp_t e S c) (subst_typ T S).
Proof.
intros.
unfold subst_exp_t; unfold subst_typ.
eapply bind_exp_t_typing.
  eassumption.
intro a; destruct a; reflexivity.
intro x; symmetry; apply bind_map_id_typ; reflexivity.
intro a; destruct a; simpl.
  assumption.
  constructor; symmetry; apply bind_map_id_typ; reflexivity.
Qed.

Lemma subst_exp_e_typing {V TV : Set} :
  forall (Gamma : env V TV) e1 e2 T1 T2,
    typing (extend_e Gamma T2) e1 T1 ->
    typing Gamma e2 T2 ->
    typing Gamma (subst_exp_e e1 e2) T1.
Proof.
intros.
unfold subst_exp_e; eapply bind_exp_e_typing.
  eassumption.
intro x; destruct x; trivial.
  constructor; reflexivity.
reflexivity.
Qed.
