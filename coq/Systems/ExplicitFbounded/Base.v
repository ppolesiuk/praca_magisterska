Require Systems.FboundedCommon.Base.
Require Systems.ExplicitFbounded.Syntax.
Require Systems.ExplicitFbounded.SyntaxMeta.
Require Systems.ExplicitFbounded.Relations.

Include Systems.FboundedCommon.Base.
Include Systems.ExplicitFbounded.Syntax.
Include Systems.ExplicitFbounded.SyntaxMeta.
Include Systems.ExplicitFbounded.Relations.

Require Import Metatheory.TypeSystem.

Definition type_system (V TV : Set) : TypeSystem :=
  {| term  := exp V TV
   ; valid := well_typed
   ; red   := beta_iota_red
  |}.
