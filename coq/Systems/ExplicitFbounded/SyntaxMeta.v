Require Import Metatheory.VarSet.
Require Import Metatheory.Misc.
Require Import Metatheory.Relations.
Require Import Systems.ExplicitFbounded.Syntax.
Require Import Systems.FboundedCommon.SyntaxMeta.

Fixpoint map_crc {TV TW : Set} (f : TV -> TW) (c : crc TV) : crc TW :=
  match c with
  | crc_id          => crc_id
  | crc_comp c1 c2  => crc_comp (map_crc f c1) (map_crc f c2)
  | crc_top         => crc_top
  | crc_var a       => crc_var (f a)
  | crc_arrow c1 c2 => crc_arrow (map_crc f c1) (map_crc f c2)
  | crc_all c1 c2   => 
      crc_all (map_crc (map_inc f) c1) (map_crc (map_inc f) c2)
  end.

Fixpoint map_exp_t {V TV TW : Set} (f : TV -> TW) (e : exp V TV)
    : exp V TW :=
  match e with
  | exp_var x      => exp_var x
  | exp_abs T e    => exp_abs (map_typ f T) (map_exp_t f e)
  | exp_app e1 e2  => exp_app (map_exp_t f e1) (map_exp_t f e2)
  | exp_tabs U e   => 
      exp_tabs (map_typ (map_inc f) U) (map_exp_t (map_inc f) e)
  | exp_tapp e T c => exp_tapp (map_exp_t f e) (map_typ f T) (map_crc f c)
  | exp_capp c e   => exp_capp (map_crc f c) (map_exp_t f e)
  end.

Fixpoint map_exp_e {V W TV : Set} (f : V -> W) (e : exp V TV) : exp W TV :=
  match e with
  | exp_var x      => exp_var (f x)
  | exp_abs T e    => exp_abs T (map_exp_e (map_inc f) e)
  | exp_app e1 e2  => exp_app (map_exp_e f e1) (map_exp_e f e2)
  | exp_tabs U e   => exp_tabs U (map_exp_e f e)
  | exp_tapp e T c => exp_tapp (map_exp_e f e) T c
  | exp_capp c e   => exp_capp c (map_exp_e f e)
  end.

Definition shift_c {TV TW : Set} (f : TV -> crc TW) (a : inc TV) 
    : crc (inc TW) :=
  match a with
  | VZ   => crc_var VZ
  | VS a => map_crc (@VS _) (f a)
  end.

Definition shift_tc {TV TW : Set} (f : TV -> typ TW * crc TW) (a : inc TV)
    : typ (inc TW) * crc (inc TW) :=
  (shift_t (f_fst f) a, shift_c (f_snd f) a).

Definition shift_et {V W TV : Set} (f : V -> exp W TV) (x : V)
    : exp W (inc TV) :=
  map_exp_t (@VS _) (f x).

Definition shift_e {V W TV : Set} (f : V -> exp W TV) (x : inc V) 
    : exp (inc W) TV :=
  match x with
  | VZ   => exp_var VZ
  | VS x => map_exp_e (@VS _) (f x)
  end.

Fixpoint bind_crc {TV TW : Set} (f : TV -> crc TW) (c : crc TV) : crc TW :=
  match c with
  | crc_id          => crc_id
  | crc_comp c1 c2  => crc_comp (bind_crc f c1) (bind_crc f c2)
  | crc_top         => crc_top
  | crc_var a       => f a
  | crc_arrow c1 c2 => crc_arrow (bind_crc f c1) (bind_crc f c2)
  | crc_all c1 c2   => 
      crc_all (bind_crc (shift_c f) c1) (bind_crc (shift_c f) c2)
  end.

Fixpoint bind_exp_t {V TV TW : Set} (f : TV -> typ TW * crc TW) (e : exp V TV)
    : exp V TW :=
  match e with
  | exp_var x      => exp_var x
  | exp_abs T e    => exp_abs (bind_typ (f_fst f) T) (bind_exp_t f e)
  | exp_app e1 e2  => exp_app (bind_exp_t f e1) (bind_exp_t f e2)
  | exp_tabs U e   => 
      exp_tabs (bind_typ (shift_t (f_fst f)) U) (bind_exp_t (shift_tc f) e)
  | exp_tapp e T c => 
      exp_tapp (bind_exp_t f e) (bind_typ (f_fst f) T) (bind_crc (f_snd f) c)
  | exp_capp c e   => exp_capp (bind_crc (f_snd f) c) (bind_exp_t f e)
  end.

Fixpoint bind_exp_e {V W TV : Set} (f : V -> exp W TV) (e : exp V TV) 
    : exp W TV :=
  match e with
  | exp_var x      => f x
  | exp_abs T e    => exp_abs T (bind_exp_e (shift_e f) e)
  | exp_app e1 e2  => exp_app (bind_exp_e f e1) (bind_exp_e f e2)
  | exp_tabs U e   => exp_tabs U (bind_exp_e (shift_et f) e)
  | exp_tapp e T c => exp_tapp (bind_exp_e f e) T c
  | exp_capp c e   => exp_capp c (bind_exp_e f e)
  end.

Definition subst_start_crc {TV : Set} (c : crc TV) (a : inc TV) : crc TV :=
  match a with
  | VZ   => c
  | VS a => crc_var a
  end.

Definition subst_start_typ_crc {TV : Set} 
    (T : typ TV) (c : crc TV) (a : inc TV) : typ TV * crc TV :=
  match a with
  | VZ   => (T, c)
  | VS a => (typ_var a, crc_var a)
  end.

Definition subst_start_exp {V TV : Set} (e : exp V TV) (x : inc V)
    : exp V TV :=
  match x with
  | VZ   => e
  | VS x => exp_var x
  end.

Definition subst_crc {TV : Set} (c : crc (inc TV)) (cs : crc TV) : crc TV :=
  bind_crc (subst_start_crc cs) c.

Definition subst_exp_t {V TV : Set} 
    (e : exp V (inc TV)) (T : typ TV) (c : crc TV)
    : exp V TV :=
  bind_exp_t (subst_start_typ_crc T c) e.

Definition subst_exp_e {V TV : Set} (e : exp (inc V) TV) (es : exp V TV) 
    : exp V TV :=
  bind_exp_e (subst_start_exp es) e.

Section ContextCl.

Variable R : forall (V TV : Set), exp V TV -> exp V TV -> Prop.

Inductive context_cl {V TV : Set} 
    : exp V TV -> exp V TV -> Prop :=
| CCL_Step : forall e e',
    R _ _ e e' ->
    context_cl e e'
| CCL_Abs  : forall T e e',
    @context_cl _ _ e e' ->
    context_cl (exp_abs T e) (exp_abs T e')
| CCL_App1 : forall e1 e1' e2,
    context_cl e1 e1' ->
    context_cl (exp_app e1 e2) (exp_app e1' e2)
| CCL_App2 : forall e1 e2 e2',
    context_cl e2 e2' ->
    context_cl (exp_app e1 e2) (exp_app e1 e2')
| CCL_TAbs : forall U e e',
    @context_cl _ _ e e' ->
    context_cl (exp_tabs U e) (exp_tabs U e')
| CCL_TApp : forall e e' T c,
    context_cl e e' ->
    context_cl (exp_tapp e T c) (exp_tapp e' T c)
| CCL_CApp : forall c e e',
    context_cl e e' ->
    context_cl (exp_capp c e) (exp_capp c e')
.

End ContextCl.

Lemma ccl_star_ccl_lemma
  (R : forall V TV : Set, exp V TV -> exp V TV -> Prop)
  {V TV : Set} : forall e1 e2 : exp V TV,
  context_cl (fun V TV => (@context_cl R V TV)^*) e1 e2 -> 
    (context_cl R)^* e1 e2.
Proof.
intros e1 e2 Hred; induction Hred; try assumption; clear Hred.
induction IHHred.
  constructor 1.
  constructor 2; apply CCL_Abs; assumption.
  econstructor 3; try eassumption; apply CCL_Abs; assumption.
induction IHHred.
  constructor 1.
  constructor 2; apply CCL_App1; assumption.
  econstructor 3; try eassumption; apply CCL_App1; assumption.
induction IHHred.
  constructor 1.
  constructor 2; apply CCL_App2; assumption.
  econstructor 3; try eassumption; apply CCL_App2; assumption.
induction IHHred.
  constructor 1.
  constructor 2; apply CCL_TAbs; assumption.
  econstructor 3; try eassumption; apply CCL_TAbs; assumption.
induction IHHred.
  constructor 1.
  constructor 2; apply CCL_TApp; assumption.
  econstructor 3; try eassumption; apply CCL_TApp; assumption.
induction IHHred.
  constructor 1.
  constructor 2; apply CCL_CApp; assumption.
  econstructor 3; try eassumption; apply CCL_CApp; assumption.
Qed.