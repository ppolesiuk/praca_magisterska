Require Import Metatheory.VarSet.

Require Export Systems.FboundedCommon.Syntax.

Inductive crc (TV : Set) : Set :=
| crc_id    : crc TV
| crc_comp  : crc TV -> crc TV -> crc TV
| crc_top   : crc TV
| crc_var   : TV -> crc TV
| crc_arrow : crc TV -> crc TV -> crc TV
| crc_all   : crc (inc TV) -> crc (inc TV) -> crc TV
.

Inductive exp (V TV : Set) : Set :=
| exp_var  : V -> exp V TV
| exp_abs  : typ TV       -> exp (inc V) TV -> exp V TV
| exp_app  : exp V TV     -> exp V TV       -> exp V TV
| exp_tabs : typ (inc TV) -> exp V (inc TV) -> exp V TV
| exp_tapp : exp V TV -> typ TV -> crc TV -> exp V TV
| exp_capp : crc TV   -> exp V TV -> exp V TV
.

Arguments crc_id    [TV].
Arguments crc_comp  [TV] _ _.
Arguments crc_top   [TV].
Arguments crc_var   [TV] _.
Arguments crc_arrow [TV] _ _.
Arguments crc_all   [TV] _ _.

Arguments exp_var  [V] [TV] _.
Arguments exp_abs  [V] [TV] _ _.
Arguments exp_app  [V] [TV] _ _.
Arguments exp_tabs [V] [TV] _ _.
Arguments exp_tapp [V] [TV] _ _ _.
Arguments exp_capp [V] [TV] _ _.
