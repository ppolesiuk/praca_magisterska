Require Import Systems.ExplicitFbounded.Base.

Lemma sub_inversion_top {TV : Set} : 
  forall Gamma (c : crc TV) T1 T2, sub Gamma c T1 T2 ->
    T1 = typ_top -> T2 = typ_top.
Proof.
induction 1; intros; subst; try discriminate; auto.
Qed.

Lemma sub_inversion_arrow {TV : Set} :
  forall Gamma (c : crc TV) T1 T2, sub Gamma c T1 T2 ->
  forall T11 T12, T1 = typ_arrow T11 T12 ->
  T2 = typ_top \/ exists T21 T22, T2 = typ_arrow T21 T22.
Proof.
induction 1; intros; subst; try discriminate; eauto.
edestruct IHsub2 as [ Htop | [ S1 [ S2 HEq ]]]; try reflexivity.
  left; eapply sub_inversion_top; eassumption.
  subst; edestruct IHsub1; eauto.
Qed.

Lemma sub_inversion_all {TV : Set} :
  forall Gamma (c : crc TV) T1 T2, sub Gamma c T1 T2 ->
  forall T11 T12, T1 = typ_all T11 T12 ->
  T2 = typ_top \/ exists T21 T22, T2 = typ_all T21 T22.
Proof.
induction 1; intros; subst; try discriminate; eauto.
edestruct IHsub2 as [ Htop | [ S1 [ S2 HEq ]]]; try reflexivity.
  left; eapply sub_inversion_top; eassumption.
  subst; edestruct IHsub1; eauto.
Qed.
