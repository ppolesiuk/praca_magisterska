Require Import Systems.ExplicitFbounded.Base.
Require Import Systems.ExplicitFbounded.Properties.

Lemma SubjectReduction_context_cl {V TV : Set} : 
  forall (R : forall V TV, exp V TV -> exp V TV -> Prop),
  (forall V TV e1 e2, R V TV e1 e2 -> forall Gamma T, 
      typing Gamma e1 T -> typing Gamma e2 T) ->
  forall (e1 e2 : exp V TV),
  context_cl R e1 e2 -> forall Gamma T, 
    typing Gamma e1 T -> typing Gamma e2 T.
Proof.
intros R RsubjectRed e1 e2 Hred; induction Hred; intros Gamma TT Htp.
eauto.
inversion_clear Htp; constructor; auto.
inversion_clear Htp; econstructor; eauto.
inversion_clear Htp; econstructor; eauto.
inversion_clear Htp; constructor; auto.
inversion_clear Htp; econstructor; eauto.
inversion_clear Htp; econstructor; eauto.
Qed.

Lemma SubjectReduction_beta0 {V TV : Set} : forall (e1 e2 : exp V TV),
  beta e1 e2 -> forall Gamma T, 
    typing Gamma e1 T -> typing Gamma e2 T.
Proof.
intros e1 e2 Hred Gamma TT Htp; induction Hred.
inversion Htp; subst.
  inversion H2; subst.
  eapply subst_exp_e_typing; eassumption.
inversion Htp; subst.
  inversion H3; subst.
  eapply subst_exp_t_typing; eassumption.
Qed.

Lemma SubjectReduction_beta {V TV : Set} : forall (e1 e2 : exp V TV),
  beta_red e1 e2 -> forall Gamma T, 
    typing Gamma e1 T -> typing Gamma e2 T.
Proof.
apply SubjectReduction_context_cl.
intros V0 TV0; apply SubjectReduction_beta0.
Qed.

Lemma SubjectReduction_iota0 {V TV : Set} : forall (e1 e2 : exp V TV),
  iota e1 e2 -> forall Gamma T, 
    typing Gamma e1 T -> typing Gamma e2 T.
Proof.
intros e1 e2 Hred Gamma TT Htp; induction Hred.
(* Iota-Id *)
inversion_clear Htp.
  inversion H0; subst; assumption.
(* Iota-Comp *)
inversion_clear Htp.
  inversion H0; subst; repeat econstructor; eassumption.
(* Iota-Arrow *)
inversion_clear Htp.
  inversion H; subst.
  inversion H5; subst.
  repeat econstructor; eassumption.
(* Iota-All *)
inversion Htp; subst.
  inversion H4; subst.
  inversion H3; subst.
  repeat econstructor.
    eassumption.
  eapply (subst_crc_sub _ _ _ (typ_var VarSet.VZ));
    eassumption.
  eapply subst_crc_sub; eassumption.
Qed.

Lemma SubjectReduction_iota {V TV : Set} : forall (e1 e2 : exp V TV),
  iota_red e1 e2 -> forall Gamma T, 
    typing Gamma e1 T -> typing Gamma e2 T.
Proof.
apply SubjectReduction_context_cl.
intros V0 TV0; apply SubjectReduction_iota0.
Qed.

Theorem SubjectReduction {V TV : Set} : forall (e1 e2 : exp V TV),
  beta_iota_red e1 e2 -> forall Gamma T, 
    typing Gamma e1 T -> typing Gamma e2 T.
Proof.
intros e1 e2 Hred Gamma T Htp; destruct Hred.
eapply SubjectReduction_beta; eassumption.
eapply SubjectReduction_iota; eassumption.
Qed.

Require Import Metatheory.TypeSystem.

Corollary EFbSubjectReduction_ts {V TV : Set} :
  TypeSystemSubjectReduction (type_system V TV).
Proof.
intros e1 Htp e2 Hred; destruct Htp; econstructor.
eapply SubjectReduction; [ eapply Hred | eassumption ].
Qed.