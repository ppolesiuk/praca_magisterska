Require Import Translation.Definitions.

Lemma tr_map_typ {TV TW : Set} : forall (T : E.typ TV) (f : TV -> TW),
  F.map_typ f (tr_typ T) = tr_typ (E.map_typ f T).
Proof.
intro T; generalize TW; clear TW; induction T; intros TW f; simpl.
reflexivity.
reflexivity.
rewrite IHT1; rewrite IHT2; reflexivity.
rewrite IHT1; rewrite IHT2; reflexivity.
Qed.

Lemma tr_map_crc_t {TV : Set} : forall (c : E.crc TV),
  forall (TW VF WF : Set) (f : VF -> WF) (g : TV -> TW)
    (T : TV -> VF) (T' : TW -> WF)
    (Hfg  : forall a, f (T a) = T' (g a)),
  F.map_exp_e f (F.map_exp_t g (tr_crc T c))
    = tr_crc T' (E.map_crc g c).
Proof.
induction c; intros; simpl.
reflexivity.
erewrite IHc1; try erewrite IHc2; try reflexivity.
  intro a; unfold shift; simpl; rewrite Hfg; reflexivity.
  intro a; unfold shift; simpl; rewrite Hfg; reflexivity.
reflexivity.
rewrite Hfg; reflexivity.
erewrite IHc1; try erewrite IHc2; try reflexivity.
  intro a; unfold shift; simpl; rewrite Hfg; reflexivity.
  intro a; unfold shift; simpl; rewrite Hfg; reflexivity.
erewrite IHc1; try erewrite IHc2; try reflexivity.
  intro a; destruct a; simpl; trivial; rewrite Hfg; reflexivity.
  intro a; destruct a; simpl; trivial; rewrite Hfg; reflexivity.
Qed.

Lemma tr_map_crc_e {TV : Set} : forall (c : E.crc TV),
  forall (VF WF : Set) (f : VF -> WF)
    (T : TV -> VF) (T' : TV -> WF)
    (HTT' : forall a, f (T a) = T' a),
  F.map_exp_e f (tr_crc T c) = tr_crc T' c.
Proof.
induction c; intros; simpl.
reflexivity.
erewrite IHc1. erewrite IHc2. reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
reflexivity.
rewrite HTT'; reflexivity.
erewrite IHc1. erewrite IHc2. reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
erewrite IHc1. erewrite IHc2. reflexivity.
  intro a; destruct a; simpl; trivial.
    rewrite HTT'; reflexivity.
  intro a; destruct a; simpl; trivial.
    rewrite HTT'; reflexivity.
Qed.

Lemma tr_map_exp_t {V TV : Set} : forall (e : E.exp V TV),
  forall (TW VF WF : Set) (f : VF -> WF) (g : TV -> TW)
    (E : V -> VF) (E' : V -> WF) (T : TV -> VF) (T' : TW -> WF)
    (HEE' : forall x, f (E x) = E' x)
    (Hfg  : forall a, f (T a) = T' (g a)),
  F.map_exp_e f (F.map_exp_t g (tr_exp E T e))
    = tr_exp E' T' (E.map_exp_t g e).
Proof.
induction e; intros; simpl.
rewrite HEE'; reflexivity.
erewrite IHe. reflexivity.
  intro x; destruct x; simpl; trivial; rewrite HEE'; reflexivity.
  intro a; unfold shift; simpl; rewrite Hfg; reflexivity.
erewrite IHe1; try erewrite IHe2; try reflexivity; assumption.
erewrite IHe; try reflexivity.
  intro x; unfold shift; simpl; rewrite HEE'; reflexivity.
  intro a; destruct a; simpl; trivial; rewrite Hfg; reflexivity.
erewrite IHe; try erewrite tr_map_crc_t; 
  try rewrite tr_map_typ; try reflexivity; assumption.
erewrite IHe; try erewrite tr_map_crc_t; try reflexivity; assumption.
Qed.

Lemma tr_map_exp_e {V TV : Set} : forall (e : E.exp V TV),
  forall (W VF WF : Set) (f : VF -> WF) (g : V -> W)
    (E : V -> VF) (E' : W -> WF) (T : TV -> VF) (T' : TV -> WF)
    (HTT' : forall a, f (T a) = T' a)
    (Hfg  : forall x, f (E x) = E' (g x)),
  F.map_exp_e f (tr_exp E T e) = tr_exp E' T' (E.map_exp_e g e).
Proof.
induction e; intros; simpl.
rewrite Hfg; reflexivity.
erewrite IHe.
  reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
  intro x; destruct x; simpl; trivial; rewrite Hfg; reflexivity.
erewrite IHe1; try erewrite IHe2; try reflexivity; assumption.
erewrite IHe.
  reflexivity.
  intro a; destruct a; simpl; trivial; rewrite HTT'; reflexivity.
  intro x; unfold shift; simpl; rewrite Hfg; reflexivity.
erewrite IHe; try erewrite tr_map_crc_e; try reflexivity; assumption.
erewrite IHe; try erewrite tr_map_crc_e; try reflexivity; assumption.
Qed.

Lemma tr_bind_typ {TV TW : Set} : 
  forall (T : E.typ TV) (f : TV -> E.typ TW) (g : TV -> F.typ TW),
  (forall a, tr_typ (f a) = g a) ->
  F.bind_typ g (tr_typ T) = tr_typ (E.bind_typ f T).
Proof.
intro T; generalize TW; clear TW; induction T; intros TW f g Hfg; simpl.
reflexivity.
auto.
rewrite (IHT1 _ f g Hfg); rewrite (IHT2 _ f g Hfg); reflexivity.
erewrite IHT1.
erewrite IHT2.
  reflexivity.
intro a; destruct a; simpl; trivial.
  rewrite <- Hfg; rewrite tr_map_typ; reflexivity.
intro a; destruct a; simpl; trivial.
  rewrite <- Hfg; rewrite tr_map_typ; reflexivity.
Qed.

Lemma tr_bind_crc_t {TV : Set} : forall (c : E.crc TV),
  forall (TW VF WF : Set)
    (fe : VF -> F.exp WF TW) (ft : TV -> F.typ TW)
    (g : TV -> E.crc TW)
    (T : TV -> VF) (T' : TW -> WF)
    (Hfg : forall a, fe (T a) = tr_crc T' (g a)),
  F.bind_exp_e fe (F.bind_exp_t ft (tr_crc T c))
    = tr_crc T' (E.bind_crc g c).
Proof.
induction c; intros; simpl.
reflexivity.
erewrite IHc1; try erewrite IHc2; try reflexivity.
  intro; unfold shift; simpl; rewrite Hfg;
    apply tr_map_crc_e; auto.
  intro; unfold shift; simpl; rewrite Hfg;
    apply tr_map_crc_e; auto.
reflexivity.
auto.
erewrite IHc1; try erewrite IHc2; try reflexivity.
  intro; unfold shift; simpl; rewrite Hfg.
    erewrite tr_map_crc_e; try erewrite tr_map_crc_e; try reflexivity.
  intro; unfold shift; simpl; rewrite Hfg.
    erewrite tr_map_crc_e; try erewrite tr_map_crc_e; try reflexivity.
erewrite IHc1; try erewrite IHc2; try reflexivity.
  intro a; destruct a; simpl; trivial.
    unfold shift; unfold F.shift_et; simpl; rewrite Hfg.
    erewrite tr_map_crc_e; try erewrite tr_map_crc_t; 
      intros; reflexivity.
  intro a; destruct a; simpl; trivial.
    unfold shift; unfold F.shift_et; simpl; rewrite Hfg.
    erewrite tr_map_crc_e; try erewrite tr_map_crc_t; 
      intros; reflexivity.
Qed.

Lemma tr_bind_crc_e {TV : Set} : forall (c : E.crc TV),
  forall (VF WF : Set) (f : VF -> F.exp WF TV)
    (T : TV -> VF) (T' : TV -> WF)
  (HTT' : forall a, f (T a) = F.exp_var (T' a)),
  F.bind_exp_e f (tr_crc T c) = tr_crc T' c.
Proof.
induction c; intros; simpl.
reflexivity.
erewrite IHc1. erewrite IHc2. reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
reflexivity.
auto.
erewrite IHc1. erewrite IHc2. reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
erewrite IHc1. erewrite IHc2. reflexivity.
  intro a; destruct a; simpl; trivial.
    unfold shift; unfold F.shift_et; simpl.
    rewrite HTT'; reflexivity.
  intro a; destruct a; simpl; trivial.
    unfold shift; unfold F.shift_et; simpl.
    rewrite HTT'; reflexivity.
Qed.

Lemma tr_bind_exp_t {V TV : Set} : forall (e : E.exp V TV),
  forall (TW VF WF : Set) 
    (fe : VF -> F.exp WF TW) (ft : TV -> F.typ TW)
    (g : TV -> E.typ TW * E.crc TW)
    (E : V -> VF) (E' : V -> WF) (T : TV -> VF) (T' : TW -> WF)
    (HEE' : forall x, fe (E x) = F.exp_var (E' x))
    (Hfeg : forall a, fe (T a) = tr_crc T' (snd (g a)))
    (Hftg : forall a, ft a = tr_typ (fst (g a))),
  F.bind_exp_e fe (F.bind_exp_t ft (tr_exp E T e))
    = tr_exp E' T' (E.bind_exp_t g e).
Proof.
induction e; intros; simpl.
rewrite HEE'; reflexivity.
erewrite IHe; try reflexivity.
  intro x; destruct x; simpl; trivial; rewrite HEE'; reflexivity.
  intro a; unfold F.shift_e; simpl; rewrite Hfeg; 
    apply tr_map_crc_e; auto.
  assumption.
erewrite IHe1; try erewrite IHe2; try reflexivity; assumption.
erewrite IHe; try reflexivity.
  intro x; unfold F.shift_et; simpl; rewrite HEE'; reflexivity.
  intro a; destruct a; simpl; trivial.
    unfold F.shift_et; simpl; rewrite Hfeg; 
    apply tr_map_crc_t; auto.
  intro a; destruct a; simpl; trivial.
    rewrite Hftg; apply tr_map_typ.
erewrite IHe; try erewrite tr_bind_typ; 
  try erewrite tr_bind_crc_t; try reflexivity; auto.
    intro. symmetry; apply Hftg.
erewrite IHe; try erewrite tr_bind_crc_t; try reflexivity; assumption.
Qed.

Lemma tr_bind_exp_e {V TV : Set} : forall (e : E.exp V TV),
  forall (W VF WF : Set) (f : VF -> F.exp WF TV) (g : V -> E.exp W TV)
    (E : V -> VF) (E' : W -> WF) (T : TV -> VF) (T' : TV -> WF)
    (HTT' : forall a, f (T a) = F.exp_var (T' a))
    (Hfg  : forall x, tr_exp E' T' (g x) = f (E x)),
  F.bind_exp_e f (tr_exp E T e) = tr_exp E' T' (E.bind_exp_e g e).
Proof.
induction e; intros; simpl.
auto.
erewrite IHe. reflexivity.
  intro a; unfold shift; simpl; rewrite HTT'; reflexivity.
  intro x; destruct x; simpl; trivial.
    rewrite <- Hfg; erewrite tr_map_exp_e; intros; reflexivity.
erewrite IHe1; try eassumption; 
  erewrite IHe2; try eassumption; reflexivity.
erewrite IHe. reflexivity.
  intro a; destruct a; simpl; trivial.
    unfold F.shift_et; rewrite HTT'; reflexivity.
  intro x; unfold E.shift_et; unfold F.shift_et; simpl.
    rewrite <- Hfg; erewrite tr_map_exp_t; intros; reflexivity.
erewrite IHe; try erewrite tr_bind_crc_e; try reflexivity; assumption.
erewrite IHe; try erewrite tr_bind_crc_e; try reflexivity; assumption.
Qed.