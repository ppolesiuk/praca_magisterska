Require Import Metatheory.VarSet.
Require Import Metatheory.Relations.
Require Import Translation.Definitions.
Require Import Translation.Meta.

Require Systems.F.MetaProperties.

Lemma tr_beta {V TV : Set} : forall e1 e2 : E.exp V TV,
  E.beta e1 e2 -> forall (VF : Set) (tr_evar : V -> VF) (tr_tvar : TV -> VF),
  F.beta_red^+ (tr_exp tr_evar tr_tvar e1) (tr_exp tr_evar tr_tvar e2).
Proof.
induction 1; intros VF tr_evar tr_tvar; simpl.

constructor 1; apply F.CCL_Step; constructor; subst.
  unfold F.subst_exp_e; unfold E.subst_exp_e.
  erewrite tr_bind_exp_e; try reflexivity.
  intro x; destruct x; reflexivity.

econstructor 2.
apply F.CCL_App1; apply F.CCL_Step; constructor.
  reflexivity.
constructor 1; apply F.CCL_Step; constructor; fold (@F.bind_exp_t); subst.
  unfold F.subst_exp_e; unfold E.subst_exp_t.
  erewrite tr_bind_exp_t; try reflexivity.
  intro x; destruct x; reflexivity.
  intro a; destruct a; reflexivity.
Qed.

Lemma tr_iota {V TV : Set} : forall e1 e2 : E.exp V TV,
  E.iota e1 e2 -> forall (VF : Set) (tr_evar : V -> VF) (tr_tvar : TV -> VF),
  F.beta_red^+ (tr_exp tr_evar tr_tvar e1) (tr_exp tr_evar tr_tvar e2).
Proof.
induction 1; intros VF tr_evar tr_tvar; simpl.

(* Iota-Id *)
constructor 1; apply F.CCL_Step; constructor; reflexivity.

(* Iota-Comp *)
constructor 1; apply F.CCL_Step; constructor.
unfold F.subst_exp_e; simpl.
  repeat erewrite tr_bind_crc_e; reflexivity.

(* Iota-Arrow *)
econstructor 2. apply F.CCL_App1; apply F.CCL_Step; constructor.
unfold F.subst_exp_e; simpl.
  repeat erewrite tr_bind_crc_e; reflexivity.
constructor 1; apply F.CCL_Step; constructor.
unfold F.subst_exp_e; simpl.
  repeat erewrite tr_bind_crc_e.
  rewrite Systems.F.MetaProperties.bind_map_id_e_exp; reflexivity.
  reflexivity.
  reflexivity.

(* Iota-All *)
econstructor 2.
  apply F.CCL_App1; apply F.CCL_TApp; apply F.CCL_Step; constructor.
  unfold F.subst_exp_e; simpl.
  repeat erewrite tr_bind_crc_e; try reflexivity.
    instantiate (1 := map_inc tr_tvar); intro a; destruct a; reflexivity.
    instantiate (1 := map_inc tr_tvar); intro a; destruct a; reflexivity.
econstructor 2. apply F.CCL_App1; apply F.CCL_Step; constructor.
  unfold F.subst_exp_t; simpl; reflexivity.
constructor 1; apply F.CCL_Step; constructor.
  subst; unfold F.subst_exp_e; unfold E.subst_crc; simpl.
  repeat erewrite tr_bind_crc_t.
  unfold F.shift_et.
  rewrite Systems.F.MetaProperties.map_et_exp_comm.
  rewrite Systems.F.MetaProperties.bind_map_id_t_exp.
  rewrite Systems.F.MetaProperties.bind_map_id_e_exp.
  reflexivity.
reflexivity.
reflexivity.
intro a; destruct a; reflexivity.
intro a; destruct a; reflexivity.
Qed.

Lemma tr_beta_red {V TV : Set} : forall e1 e2 : E.exp V TV,
  E.beta_red e1 e2 -> forall (VF : Set)
    (tr_evar : V -> VF) (tr_tvar : TV -> VF),
  F.beta_red^+ (tr_exp tr_evar tr_tvar e1) (tr_exp tr_evar tr_tvar e2).
Proof.
induction 1; intros; simpl.
apply tr_beta; assumption.
apply F.ccl_plus_ccl_lemma; apply F.CCL_Abs; apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_App1; apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_App2; apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_TAbs; apply F.CCL_Abs;
  apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_App1; apply F.CCL_TApp;
  apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_App2; apply F.CCL_Step; auto.
Qed.

Lemma tr_iota_red {V TV : Set} : forall e1 e2 : E.exp V TV,
  E.iota_red e1 e2 -> forall (VF : Set)
    (tr_evar : V -> VF) (tr_tvar : TV -> VF),
  F.beta_red^+ (tr_exp tr_evar tr_tvar e1) (tr_exp tr_evar tr_tvar e2).
Proof.
induction 1; intros; simpl.
apply tr_iota; assumption.
apply F.ccl_plus_ccl_lemma; apply F.CCL_Abs; apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_App1; apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_App2; apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_TAbs; apply F.CCL_Abs;
  apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_App1; apply F.CCL_TApp;
  apply F.CCL_Step; auto.
apply F.ccl_plus_ccl_lemma; apply F.CCL_App2; apply F.CCL_Step; auto.
Qed.

Lemma tr_beta_iota_red {V TV : Set} : forall e1 e2 : E.exp V TV,
  E.beta_iota_red e1 e2 -> forall (VF : Set)
    (tr_evar : V -> VF) (tr_tvar : TV -> VF),
  F.beta_red^+ (tr_exp tr_evar tr_tvar e1) (tr_exp tr_evar tr_tvar e2).
Proof.
induction 1.
  apply tr_beta_red; assumption.
  apply tr_iota_red; assumption.
Qed.