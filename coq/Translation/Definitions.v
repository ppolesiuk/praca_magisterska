Require Import Metatheory.VarSet.
Require Systems.ExplicitFbounded.Base.
Require Systems.F.Base.

Module E := Systems.ExplicitFbounded.Base.
Module F := Systems.F.Base.

Definition F_typ_unit (TV : Set) : F.typ TV :=
  F.typ_all (F.typ_arrow (F.typ_var VZ) (F.typ_var VZ)).

Definition F_exp_unit (V TV : Set) : F.exp V TV :=
  F.exp_tabs (F.exp_abs (F.exp_var VZ)).

Fixpoint tr_typ {TV : Set} (T : E.typ TV) : F.typ TV :=
  match T with
  | E.typ_top         => F_typ_unit TV
  | E.typ_var a       => F.typ_var a
  | E.typ_arrow T1 T2 => F.typ_arrow (tr_typ T1) (tr_typ T2)
  | E.typ_all U T     =>
      F.typ_all (F.typ_arrow
        (F.typ_arrow (F.typ_var VZ) (tr_typ U))
        (tr_typ T))
  end.

Definition shift {A B : Set} (f : A -> B) (x : A) : inc B :=
  VS (f x).

Fixpoint tr_crc {TV VF : Set}
    (tr_tvar : TV -> VF) (c : E.crc TV) : F.exp VF TV :=
  match c with
  | E.crc_id          => F.exp_abs (F.exp_var VZ)
  | E.crc_comp c1 c2  => F.exp_abs
      (F.exp_app (tr_crc (shift tr_tvar) c1)
      (F.exp_app (tr_crc (shift tr_tvar) c2)
        (F.exp_var VZ)))
  | E.crc_top         => F.exp_abs (F_exp_unit _ _)
  | E.crc_var a       => F.exp_var (tr_tvar a)
  | E.crc_arrow c1 c2 => F.exp_abs (F.exp_abs
      (F.exp_app (tr_crc (shift (shift tr_tvar)) c2)
      (F.exp_app (F.exp_var (VS VZ))
      (F.exp_app (tr_crc (shift (shift tr_tvar)) c1)
        (F.exp_var VZ)))))
  | E.crc_all c1 c2   => F.exp_abs (F.exp_tabs (F.exp_abs
      (F.exp_app (tr_crc (map_inc (shift tr_tvar)) c2)
      (F.exp_app (F.exp_tapp (F.exp_var (VS VZ)) (F.typ_var VZ))
      (tr_crc (map_inc (shift tr_tvar)) c1)))))
  end.

Fixpoint tr_exp {V TV VF : Set}
    (tr_evar : V -> VF) (tr_tvar : TV -> VF) (e : E.exp V TV) : F.exp VF TV :=
  match e with
  | E.exp_var x      => F.exp_var (tr_evar x)
  | E.exp_abs T e    => F.exp_abs (tr_exp (map_inc tr_evar) (shift tr_tvar) e)
  | E.exp_app e1 e2  => F.exp_app 
      (tr_exp tr_evar tr_tvar e1) (tr_exp tr_evar tr_tvar e2)
  | E.exp_tabs U e   => F.exp_tabs (F.exp_abs (
      (tr_exp (shift tr_evar) (map_inc tr_tvar) e)))
  | E.exp_tapp e T c => F.exp_app
      (F.exp_tapp (tr_exp tr_evar tr_tvar e) (tr_typ T))
      (tr_crc tr_tvar c)
  | E.exp_capp c e   => F.exp_app
      (tr_crc tr_tvar c) (tr_exp tr_evar tr_tvar e)
  end.