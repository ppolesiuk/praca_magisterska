Require Import Metatheory.VarSet.
Require Import Translation.Definitions.
Require Import Translation.Meta.

Require Systems.FboundedCommon.MetaProperties.

Definition tr_tenv_env {TV VF : Set}
    (Gamma : E.tenv TV) (Sigma : F.env VF TV)
    (tr_tvar : TV -> VF) : Prop :=
  forall a : TV, Sigma (tr_tvar a) = 
    (F.typ_arrow (F.typ_var a) (tr_typ (Gamma a))).

Definition tr_eenv_env {V TV VF : Set}
    (Gamma : E.eenv V TV) (Sigma : F.env VF TV)
    (tr_evar : V -> VF) : Prop :=
  forall x : V, Sigma (tr_evar x) = tr_typ (Gamma x).

Record tr_env {V TV VF : Set}
    (Gamma : E.env V TV) (Sigma : F.env VF TV)
    (tr_evar : V -> VF) (tr_tvar : TV -> VF) : Prop :=
  { tr_eenv : tr_eenv_env (E.env_e Gamma) Sigma tr_evar
  ; tr_tenv : tr_tenv_env (E.env_t Gamma) Sigma tr_tvar
  }.

Arguments tr_eenv [V] [TV] [VF] [Gamma] [Sigma] [tr_evar] [tr_tvar] _ _.
Arguments tr_tenv [V] [TV] [VF] [Gamma] [Sigma] [tr_evar] [tr_tvar] _ _.

Lemma tr_crc_typing_aux {TV : Set} :
  forall T : E.typ (inc TV),
    F.bind_typ (F.subst_start_typ (F.typ_var VZ))
     (F.map_typ (map_inc (@VS TV)) (tr_typ T)) = tr_typ T.
Proof.
intro T.
rewrite tr_map_typ.
rewrite (tr_bind_typ _ (E.subst_start_typ (E.typ_var VZ))).
rewrite Systems.FboundedCommon.MetaProperties.bind_map_typ.
erewrite Systems.FboundedCommon.MetaProperties.bind_typ_ext.
erewrite Systems.FboundedCommon.MetaProperties.bind_return_typ.
reflexivity.
intro a; destruct a; reflexivity.
intro a; destruct a; reflexivity.
Qed.

Lemma tr_crc_typing {TV : Set} :
  forall (Gamma : E.tenv TV) c T1 T2, E.sub Gamma c T1 T2 ->
  forall (VF : Set) Sigma (tr_tvar : TV -> VF),
  tr_tenv_env Gamma Sigma tr_tvar ->
  F.typing Sigma (tr_crc tr_tvar c) 
    (F.typ_arrow (tr_typ T1) (tr_typ T2)).
Proof.
induction 1; intros VF Sigma tr_tvar Henv; simpl.
repeat constructor.
repeat econstructor; auto.
repeat constructor.
repeat constructor; subst; auto.
repeat econstructor; auto.
repeat econstructor; fold (@F.bind_typ); fold (@F.map_typ).
  rewrite tr_crc_typing_aux; apply IHsub2.
    intro a; destruct a; simpl; trivial.
    rewrite <- tr_map_typ.
    unfold F.extend_t; unfold F.extend; simpl.
    rewrite Henv; reflexivity.
  rewrite tr_crc_typing_aux; apply IHsub1.
    intro a; destruct a; simpl; trivial.
    rewrite <- tr_map_typ.
    unfold F.extend_t; unfold F.extend; simpl.
    rewrite Henv; reflexivity.
Qed.

Lemma tr_exp_typing {V TV : Set} :
  forall (Gamma : E.env V TV) e T, E.typing Gamma e T ->
  forall (VF : Set) Sigma (tr_evar : V -> VF) (tr_tvar : TV -> VF),
  tr_env Gamma Sigma tr_evar tr_tvar ->
  F.typing Sigma (tr_exp tr_evar tr_tvar e) (tr_typ T).
Proof.
induction 1; intros VF Sigma tr_evar tr_var Henv; simpl.
constructor; destruct Henv; subst; auto.
constructor; apply IHtyping; split.
  intro x; destruct x; simpl; destruct Henv; auto.
  intro a; unfold F.extend; unfold shift.
    rewrite (tr_tenv Henv); reflexivity.
econstructor; eauto.
constructor; constructor; apply IHtyping; split.
  intro x; unfold F.extend; unfold shift; unfold F.extend_t.
    rewrite (tr_eenv Henv); rewrite tr_map_typ; reflexivity.
  intro a; destruct a; simpl; trivial.
  unfold F.extend_t; rewrite (tr_tenv Henv); simpl.
  rewrite tr_map_typ; reflexivity.
repeat econstructor.
  apply IHtyping; assumption.
  fold (@tr_typ); subst; unfold E.subst_typ; unfold F.subst_typ; simpl.
    repeat erewrite tr_bind_typ.
      reflexivity.
    intro a; destruct a; simpl; reflexivity.
    instantiate (1 := E.subst_start_typ S).
    intro a; destruct a; simpl; reflexivity.
  destruct Henv; eapply tr_crc_typing; eassumption.
econstructor.
  destruct Henv; eapply tr_crc_typing; eassumption.
  auto.
Qed.