Require Import Metatheory.TypeSystem.
Require Import Metatheory.Simulation.
Require Import Translation.Definitions.
Require Import Translation.Reduction.
Require Import Translation.Typing.

Definition trans_sim (V TV : Set)
  (e : E.exp V TV) (f : F.exp (V + TV) TV) : Prop :=
  f = tr_exp inl inr e.

Definition trans_env {V TV : Set} (Gamma : E.env V TV) : F.env (V + TV) TV :=
  fun x => match x with
  | inl x => tr_typ (E.env_e Gamma x)
  | inr a => F.typ_arrow (F.typ_var a) (tr_typ (E.env_t Gamma a))
  end.

Lemma EFbSimulation {V TV : Set} :
  Simulation E.beta_iota_red F.beta_red (trans_sim V TV).
Proof.
intros e1 e2 f1 Hred Hsim; rewrite Hsim.
eexists; split.
  apply tr_beta_iota_red; eassumption.
  reflexivity.
Qed.

Theorem EFbStrongNorm_ts {V TV : Set} : 
  TypeSystemStrongNorm (E.type_system V TV).
Proof.
intros e Htp; destruct Htp as [Gamma T Htp].
eapply SimulationLemma.
  apply EFbSimulation.
  reflexivity.

apply F.F_StrongNorm.
econstructor; eapply tr_exp_typing.
  eassumption.
instantiate (1 := trans_env Gamma); split; intro; reflexivity.
Qed.